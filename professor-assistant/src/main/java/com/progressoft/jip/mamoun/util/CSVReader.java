package com.progressoft.jip.mamoun.util;

import com.progressoft.jip.mamoun.model.ScientificData;
//import com.sun.istack.internal.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CSVReader implements CustomReader {
    private final BufferedReader bufferedReader;
    private List<String> header = null;
    private List<List<String>> records = null;

    public CSVReader(BufferedReader bufferedReader) {
        if (bufferedReader == null) {
            throw new IllegalArgumentException("Please provide good buffer reader or use the reader factory");
        }
        this.bufferedReader = bufferedReader;
    }

//    @NotNull
    @Override
    public ScientificData getData() {
        if (header == null || records == null) {
            try {
                String line = bufferedReader.readLine();
                header = new ArrayList<>();
                header.addAll(Arrays.asList(line.replaceAll(" ", "").split(",")));
                records = new ArrayList<>();
                while ((line = bufferedReader.readLine()) != null) {
                    records.add(Arrays.asList(line.replaceAll(" ", "").split(",")));
                }
                return new ScientificData(header, records);
            } catch (IOException e) {
                throw new RuntimeException("There was an error parsing the file you entered");
            }
        }
        return new ScientificData(header, records);

    }

    public BufferedReader getBufferedReader() {
        return bufferedReader;
    }
}
