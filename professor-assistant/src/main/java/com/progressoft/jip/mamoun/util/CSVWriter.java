package com.progressoft.jip.mamoun.util;

import com.progressoft.jip.mamoun.model.ScientificData;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

public class CSVWriter implements CustomWriter {

    private final Path outPath;

    public CSVWriter(String outFolder, String fileName) {
        this.outPath = Paths.get((outFolder != null ? outFolder + "/" : "") + (fileName != null ? fileName : LocalDateTime.now()) + ".csv");
        if (outFolder != null) {
            // TODO, use NIO instead of File (DONE)
            Path outerFolder = Paths.get(outFolder);
            if (Files.notExists(outerFolder)) {
                try {
                    Files.createDirectories(outerFolder);
                } catch (IOException e) {
                    throw new RuntimeException("Couldn't create dir");
                }
            }
        }

    }

    @Override
    public void writeToFile(ScientificData scientificData) {
        try {
            Files.write(outPath, Collections.singleton(String.join(", ", scientificData.getHeader())));
            for (List<String> record : scientificData.getRecords()) {
                Files.write(outPath, Collections.singleton(String.join(", ", record)), StandardOpenOption.APPEND);
            }
            System.out.println("\nFile saved successfully in '" + this.outPath + "'");

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
