package com.progressoft.jip.mamoun.model;

import java.util.ArrayList;
import java.util.List;

public class ScientificData {

    private final List<String> header;
    private final List<List<String>> records;


    public ScientificData(List<String> header, List<List<String>> records) {
        if (header == null)
            throw new IllegalArgumentException("Null header value");
        if (records == null)
            throw new IllegalArgumentException("Null records");
        if (header.size() == 0)
            throw new IllegalArgumentException("Empty header");
        if (records.size() == 0)
            throw new IllegalArgumentException("Empty records");
        if (records.get(0).size() != header.size())
            throw new IllegalArgumentException("Length of records and header mismatch");
        this.header = header;
        this.records = records;
    }


    // TODO, return the header as a new ArrayList or any other wanted List Implementation (DONE)
    public List<String> getHeader() {
        return new ArrayList<>(header);
    }

    // TODO, return the header as a new ArrayList or any other wanted List Implementation (DONE)
    public List<List<String>> getRecords() {
        return new ArrayList<>(records);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ScientificData)) return false;

        ScientificData that = (ScientificData) o;

        if (!getHeader().equals(that.getHeader())) return false;
        return getRecords().equals(that.getRecords());
    }

    @Override
    public int hashCode() {
        int result = getHeader().hashCode();
        result = 31 * result + getRecords().hashCode();
        return result;
    }

}
