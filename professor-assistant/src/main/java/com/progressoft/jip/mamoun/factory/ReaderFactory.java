package com.progressoft.jip.mamoun.factory;

import com.progressoft.jip.mamoun.util.CSVReader;
import com.progressoft.jip.mamoun.util.CustomReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Properties;

public class ReaderFactory {

    private static HashMap<String, CustomReader> supportedExtensions;


    // TODO Optional, Create a file inside resources folder and call it supportedExtensions.properties
    //  use Properties class to read and load the file, Properties class can be used as a hashmap after loading the resource file
    //  finally create a list from the value returned, and use it to validate the supported extensions added to the file (DONE)

    public static CustomReader getReader(Path path) throws IllegalArgumentException {
        if (path == null)
            throw new IllegalArgumentException("Path is null");

        BufferedReader bufferedReader = getBufferedReaderIfValidPath(path);
        // TODO, methods that starts with 'is' supposed to return a boolean (DONE)
        String extension = getExtension(path);

        if (supportedExtensions == null)
            setSupportedExtensions(bufferedReader);

        if (supportedExtensions.containsKey(extension))
            return supportedExtensions.get(extension);
        else
            throw new IllegalArgumentException("Not supported file extension");

    }


    private static BufferedReader getBufferedReaderIfValidPath(Path path) throws IllegalArgumentException {
        BufferedReader bufferedReader;
        try {
            bufferedReader = Files.newBufferedReader(path);
            return bufferedReader;
        } catch (IOException e) {
            throw new IllegalArgumentException("File Not Found");
        }
    }


    private static String getExtension(Path path) throws IllegalArgumentException {
        String extension;
        int i = path.toString().lastIndexOf('.');
        if (i > 0) {
            extension = path.toString().substring(i + 1);
            return extension;
        }
        throw new IllegalArgumentException("Missing file extension");
    }


    private static void setSupportedExtensions(BufferedReader bufferedReader) {
        supportedExtensions = new HashMap<>();
        Properties properties = new Properties();
        try {
            String fileName = "supportedExtensions.properties";
            properties.load(ReaderFactory.class.getClassLoader().getResourceAsStream(fileName));
            String[] extension = properties.get("supported_extensions").toString().trim().toLowerCase().split(",");
            for (String s : extension) {
                switch (s.trim()) {
                    case "csv":
                        supportedExtensions.put(s, new CSVReader(bufferedReader));
                        break;
                    case "json":
                        // supportedExtensions.put(s, )
                        break;
                    default:
                        throw new IllegalArgumentException("The entered extension in supported_extensions is not supported yet");
                }
            }

        } catch (IOException e) {
            throw new RuntimeException("Please add supportedExtensions.properties to resource file");
        }
    }

}
