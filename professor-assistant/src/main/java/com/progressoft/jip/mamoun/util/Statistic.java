package com.progressoft.jip.mamoun.util;

import com.progressoft.jip.mamoun.model.ScientificData;
import com.progressoft.jip.mamoun.model.StatisticModel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class Statistic {

    public static final HashMap<Integer, StatisticModel> previousCalculation = new HashMap<>();

    public static BigDecimal mean(List<Double> data) {
        checkData(data);
        BigDecimal mean;
        if (previousCalculation.containsKey(data.hashCode())) {
            mean = previousCalculation.get(data.hashCode()).getMean();
            if (mean != null) {
                return mean;
            }
        }

        previousCalculation.putIfAbsent(data.hashCode(), new StatisticModel());
        double sum = 0;
        for (double entry : data) {
            sum += entry;
        }
        mean = new BigDecimal(sum / data.size()).setScale(2, RoundingMode.HALF_EVEN);
        previousCalculation.get(data.hashCode()).setMean(mean);
        return mean;
    }


    public static BigDecimal standardDeviation(List<Double> data) {
        checkData(data);
        BigDecimal standardDeviation;
        if (previousCalculation.containsKey(data.hashCode())) {
            standardDeviation = previousCalculation.get(data.hashCode()).getStandardDeviation();
            if (standardDeviation != null) {
                return standardDeviation;
            }
        }
        previousCalculation.putIfAbsent(data.hashCode(), new StatisticModel());
        standardDeviation = BigDecimal.valueOf(Math.sqrt(variance(data).doubleValue())).setScale(2, RoundingMode.HALF_EVEN);
        previousCalculation.get(data.hashCode()).setStandardDeviation(standardDeviation);
        return standardDeviation;
    }


    public static BigDecimal variance(List<Double> data) {
        checkData(data);
        BigDecimal variance;
        if (previousCalculation.containsKey(data.hashCode())) {
            variance = previousCalculation.get(data.hashCode()).getVariance();
            if (variance != null) {
                return variance;
            }
        }

        previousCalculation.putIfAbsent(data.hashCode(), new StatisticModel());
        double mean = mean(data).doubleValue();
        double sum = 0;
        for (double entry : data) {
            double diff = entry - mean;
            double squared = Math.pow(diff, 2);
            sum += squared;
        }
        variance = new BigDecimal(sum / data.size()).setScale(2, RoundingMode.HALF_EVEN);
        previousCalculation.get(data.hashCode()).setVariance(variance);
        return variance;
    }


    public static BigDecimal median(List<Double> data) {
        checkData(data);
        BigDecimal median;
        if (previousCalculation.containsKey(data.hashCode())) {
            median = previousCalculation.get(data.hashCode()).getMedian();
            if (median != null) {
                return median;
            }
        }
        int dataSize = data.size();
        int middleIndex = dataSize / 2;
        Collections.sort(data);
        median = BigDecimal.valueOf(data.get(middleIndex));

        if (dataSize % 2 == 0)
            median = median.add(BigDecimal.valueOf(data.get(middleIndex - 1))).divide(BigDecimal.valueOf(2), RoundingMode.HALF_EVEN);

        previousCalculation.put(data.hashCode(), new StatisticModel());
        previousCalculation.get(data.hashCode()).setMedian(median);
        return median;

    }

    private static void checkData(List<Double> data) {
        if (data == null) throw new IllegalArgumentException("Data must not be null");
    }

}
