package com.progressoft.jip.mamoun.util;

import com.progressoft.jip.mamoun.model.ScientificData;


@FunctionalInterface
public interface CustomReader {


    ScientificData getData();
}
