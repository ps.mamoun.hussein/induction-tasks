package com.progressoft.jip.mamoun;

import com.progressoft.jip.mamoun.factory.ReaderFactory;
import com.progressoft.jip.mamoun.model.ScientificData;
import com.progressoft.jip.mamoun.util.*;
//import com.sun.istack.internal.NotNull;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    private static ZScore zScore = null;
    private static CustomReader reader = null;


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        readFile(sc);
        start(sc);
        // TODO, try to make the function recursive (DONE)
    }

    private static void start(Scanner sc) {
        printMainMenu();
        switch (sc.next()) {
            case "1":
                printSummary(zScore.getDataNumber());
                start(sc);
                break;
            case "2":
                printSummary(getzScoreForClass(sc).getDataNumber());
                start(sc);
                break;
            case "3":
                printZScore(zScore.getOutData());
                start(sc);
                break;
            case "4":
                printZScore(getzScoreForClass(sc).getOutData());
                start(sc);
                break;
            case "5":
                categoriesStudent(zScore, sc);
                start(sc);
                break;
            case "6":
                categoriesStudent(getzScoreForClass(sc), sc);
                start(sc);
                break;
            case "7":
                sc.close();
                System.out.println("App finished");
                break;
            default:
                System.out.println("Please choose from ");
                start(sc);
                break;
        }

    }

    private static void readFile(Scanner scanner) {
        try {
            System.out.println("\nPlease Enter the file path");
            String filePath = scanner.nextLine();
            Path path = Paths.get(filePath);
            reader = ReaderFactory.getReader(path);
            zScore = new ZScore(reader.getData(), "mark");
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            readFile(scanner);
        }
    }


    private static void printMainMenu() {
        System.out.println("\n1- Summary");
        System.out.println("2- Summary for a specific class");
        System.out.println("3- Display Z-scores");
        System.out.println("4- Display Z-scores for specific class");
        System.out.println("5- Categorize students");
        System.out.println("6- Categorize students in a specific class");
        System.out.println("7- Exit\n");
    }


    private static ScientificData getDataForClass(String className) {
        List<List<String>> records = new ArrayList<>();
        for (List<String> record : reader.getData().getRecords()) {
            if (record.contains(className)) {
                records.add(record);
            }
        }
        return new ScientificData(reader.getData().getHeader(), records);
    }

    // TODO, Save the results somewhere inside the memory and if the method is repeated, show the old results if it is an old class (DONE)
    private static void printSummary(List<Double> data) {
        System.out.println("Median: " + Statistic.median(data));
        System.out.println("Variance: " + Statistic.variance(data));
        System.out.println("Standard Deviation: " + Statistic.standardDeviation(data));
        System.out.println("Total count: " + data.size());
    }


    private static void printZScore(ScientificData data) {
        System.out.println(data.getHeader());
        for (List<String> record : data.getRecords()) {
            System.out.println(record);

        }
    }

    private static ZScore getzScoreForClass(Scanner scanner) {
        System.out.println("Please enter class name");
        String className = scanner.next();
        try {
            return new ZScore(getDataForClass(className), "mark");
        } catch (IllegalArgumentException e) {
            System.out.println("There is no class with this name please try again with different class name\n");
            return getzScoreForClass(scanner);
        }
    }


    // T.O.D.O : change the compared value
    private static void categoriesStudent(ZScore score, Scanner scanner) {
        int eliteCount = 0;
        int passedCount = 0;
        int failedCount = 0;
        double eliteDev = -1;
        double failedDev = 0;
        while (eliteDev < failedDev) {
            if (eliteDev != -1)
                System.out.println("make sure the elite is higher than the failed");
            System.out.println("Please Enter elite dev");
            eliteDev = scanner.nextDouble();
            System.out.println("Please Enter failed dev");
            failedDev = scanner.nextDouble();
        }
        List<String> header = new ArrayList<>(score.getOutData().getHeader());
        header.add("category");
        List<List<String>> records = new ArrayList<>();
        for (int i = 0; i < score.getDataNumber().size(); i++) {
            records.add(new ArrayList<>(score.getOutData().getRecords().get(i)));
            if (score.getDataNumber().get(i) > eliteDev) {
                records.get(i).add("Elite");
                eliteCount++;
            } else if (score.getDataNumber().get(i) < failedDev) {
                records.get(i).add("Failed");
                failedCount++;
            } else {
                records.get(i).add("Passed");
                passedCount++;
            }
        }
        printSummary(score.getDataNumber());
        System.out.println("Elite students count: " + eliteCount);
        System.out.println("Passed students count: " + passedCount);
        System.out.println("Failed students count: " + failedCount);
        // T.O.D.O change this static value when you get the idea
        System.out.println("Passing score: " + failedDev);
        System.out.println("Elite score: " + eliteDev);

        System.out.println("\nDo you want to save the results to a file (y/N) ");

        scanner.nextLine();
        String answer = scanner.nextLine();

        if (answer.equalsIgnoreCase("y")) {
            saveFile(new ScientificData(header, records));
        }
    }

    private static void saveFile(ScientificData data) {
        String path = "out-file";
        String fileName = "prof-assistant-Category-" + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        CustomWriter customWriter = new CSVWriter(path, fileName);
        customWriter.writeToFile(data);
    }


}
