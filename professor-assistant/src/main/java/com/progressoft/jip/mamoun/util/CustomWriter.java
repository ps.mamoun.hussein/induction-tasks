package com.progressoft.jip.mamoun.util;


import com.progressoft.jip.mamoun.model.ScientificData;

@FunctionalInterface
public interface CustomWriter {

    void writeToFile(ScientificData scientificData);
}
