package com.progressoft.jip.mamoun.model;

import java.math.BigDecimal;

public class StatisticModel {
    private BigDecimal mean;
    private BigDecimal standardDeviation;
    private BigDecimal variance;
    private BigDecimal median;

    public BigDecimal getMean() {
        return mean;
    }

    public void setMean(BigDecimal mean) {
        checkDataAvailability(this.mean);
        this.mean = mean;
    }

    public BigDecimal getStandardDeviation() {
        return standardDeviation;
    }

    public void setStandardDeviation(BigDecimal standardDeviation) {
        checkDataAvailability(this.standardDeviation);
        this.standardDeviation = standardDeviation;
    }

    public BigDecimal getVariance() {
        return variance;
    }

    public void setVariance(BigDecimal variance) {
        checkDataAvailability(this.variance);
        this.variance = variance;
    }

    public BigDecimal getMedian() {
        return median;
    }

    public void setMedian(BigDecimal median) {
        checkDataAvailability(this.median);
        this.median = median;
    }

    private void checkDataAvailability(BigDecimal data) {
        if (data != null) {
            throw new IllegalArgumentException("Data should not be assigned more than once");
        }
    }
}
