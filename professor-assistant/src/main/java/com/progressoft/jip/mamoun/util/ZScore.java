package com.progressoft.jip.mamoun.util;

import com.progressoft.jip.mamoun.model.ScientificData;
import com.progressoft.jip.mamoun.util.Statistic;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class ZScore {
    private final ScientificData inputData;
    private int columnIndex = -1;
    private ScientificData outData = null;
    private final List<Double> dataNumber = new ArrayList<>();


    public ZScore(ScientificData data, String normalizedColumnName) throws IllegalArgumentException {
        if (data == null)
            throw new IllegalArgumentException("Data must not be null");
        if (normalizedColumnName == null)
            throw new IllegalArgumentException("Column name must not be null");
        this.inputData = data;
        checkIfColumnIsValid(normalizedColumnName);
    }


    private void checkIfColumnIsValid(String columnName) throws IllegalArgumentException {
        List<String> header = inputData.getHeader();
        for (int i = 0; i < header.size(); i++) {
            String head = header.get(i);
            if (columnName.equalsIgnoreCase(head)) {
                checkIfAllDataAreNumberAndExist(i);
                columnIndex = i;
            }
        }
        if (columnIndex == -1)
            throw new IllegalArgumentException("Selected column does not exist");

    }

    private void checkIfAllDataAreNumberAndExist(int columnIndex) throws IllegalArgumentException {
        int dataLength = inputData.getRecords().get(0).size();
        for (int j = 0; j < inputData.getRecords().size(); j++) {
            List<String> record = inputData.getRecords().get(j);
            if (record.size() == dataLength) {
                for (int i = 0; i < record.size(); i++) {
                    if (i == columnIndex) {
                        try {
                            Double number = Double.parseDouble(record.get(i));
                            dataNumber.add(number);
                        } catch (NumberFormatException e) {
                            throw new IllegalArgumentException("Not all data belong to this column are number");
                        }
                    }

                }
            } else throw new IllegalArgumentException("Missing data on line " + j);
        }
    }

    public ScientificData getOutData() {
        if (outData == null)
            outData = new ScientificData(addHeader(), calculateData());
        return outData;
    }

    private List<List<String>> calculateData() {
        List<List<String>> data = new ArrayList<>();
        BigDecimal mean = Statistic.mean(dataNumber);
        BigDecimal standardDeviation = Statistic.standardDeviation(dataNumber);
        for (List<String> record : this.inputData.getRecords()) {
            double zValue = new BigDecimal(record.get(columnIndex)).subtract(mean).divide(standardDeviation, RoundingMode.HALF_EVEN).setScale(2, RoundingMode.HALF_EVEN).doubleValue();
            List<String> recordList = new ArrayList<>(record);
            recordList.add(columnIndex + 1, String.valueOf(zValue));
            data.add(recordList);
        }
        return data;
    }

    private List<String> addHeader() {
        List<String> header = new ArrayList<>(inputData.getHeader());
        header.add(columnIndex + 1, "z-score");
        return header;
    }

    public List<Double> getDataNumber() {
        return dataNumber;
    }
}
