package com.progressoft.jip.mamoun.util;

import com.progressoft.jip.mamoun.factory.ReaderFactory;
import com.progressoft.jip.mamoun.model.ScientificData;
import com.progressoft.jip.mamoun.util.CustomReader;
import com.progressoft.jip.mamoun.util.ZScore;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class ZScoreTest {


    @Test
    public void givenNullParams_whenConstruct_thenIllegalArgumentException() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> new ZScore(null, "mark"));
        assertEquals("Data must not be null", exception.getMessage());
        List<String> header = Arrays.asList("Student_id", "class_no", "mark");
        List<List<String>> record = Arrays.asList(Arrays.asList("202008001", "A", "60"), Arrays.asList("202008002", "A", "60"), Arrays.asList("202008003", "B", "70"));
        exception = assertThrows(IllegalArgumentException.class, () -> new ZScore(new ScientificData(header, record), null));
        assertEquals("Column name must not be null", exception.getMessage());

    }


    @Test
    public void givenInvalidColumnName_whenConstruct_thenIllegalArgumentException() {
        Path path = Paths.get("src/test/resources/dummy.csv");
        CustomReader reader = ReaderFactory.getReader(path);
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> new ZScore(reader.getData(), "invalid_header"));
        assertEquals("Selected column does not exist", exception.getMessage());
        exception = assertThrows(IllegalArgumentException.class, () -> new ZScore(reader.getData(), "class_no"));
        assertEquals("Not all data belong to this column are number", exception.getMessage());
    }

    @Test
    public void givenValidData_thenHappyScenario() {
        Path path = Paths.get("src/test/resources/dummy.csv");
        CustomReader reader = ReaderFactory.getReader(path);
        ZScore zScore = new ZScore(reader.getData(), "mark");
        ScientificData scientificData = zScore.getOutData();
        List<String> header = Arrays.asList("Student_id", "class_no", "mark", "z-score");
        List<List<String>> records = Arrays.asList(Arrays.asList("202008001", "A", "60", "-0.71"), Arrays.asList("202008002", "A", "60", "-0.71"), Arrays.asList("202008003", "B", "70", "1.42"));
        ScientificData expectedData = new ScientificData(header, records);
        assertEquals(expectedData, scientificData);

    }
}
