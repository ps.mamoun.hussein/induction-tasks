package com.progressoft.jip.mamoun.factory;

import com.progressoft.jip.mamoun.util.CSVReader;
import com.progressoft.jip.mamoun.util.CustomReader;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

class ReaderFactoryTest {

    @Test
    public void givenInvalidPath_whenCallingFactory_thenIllegalArgumentsException() {
        Path path1 = Paths.get("invalid path");
        IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class,
                () -> ReaderFactory.getReader(path1));
        assertEquals("File Not Found", thrown.getMessage());


        Path path2 = null;
        thrown = assertThrows(IllegalArgumentException.class,
                () -> ReaderFactory.getReader(path2));
        assertEquals("Path is null", thrown.getMessage());


        Path path3 = Paths.get("src/test/resources/invalidExtension");
        thrown = assertThrows(IllegalArgumentException.class,
                () -> ReaderFactory.getReader(path3));
        assertEquals("Missing file extension", thrown.getMessage());

        Path path4 = Paths.get("src/test/resources/dummy.json");
        thrown = assertThrows(IllegalArgumentException.class,
                () -> ReaderFactory.getReader(path4));
        assertEquals("Not supported file extension", thrown.getMessage());


    }


    @Test
    public void givenValidInput_whenCallingFactory_thenHappyScenario() {
        Path csvPath = Paths.get("src/test/resources/dummy.csv");
        CustomReader customReader = ReaderFactory.getReader(csvPath);
        assertNotNull(customReader);
        assertTrue(customReader instanceof CSVReader, "supposed to be csv reader");
        assertNotNull(((CSVReader) customReader).getBufferedReader());
    }


}