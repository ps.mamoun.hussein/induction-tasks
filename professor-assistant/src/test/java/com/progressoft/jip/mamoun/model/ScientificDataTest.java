package com.progressoft.jip.mamoun.model;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ScientificDataTest {

    @Test
    public void givenNullParams_whenConstruct_thenFail() {
        IllegalArgumentException thrown;

        thrown = assertThrows(IllegalArgumentException.class, () -> new ScientificData(null, new ArrayList<>()));
        assertEquals("Null header value", thrown.getMessage());

        thrown = assertThrows(IllegalArgumentException.class, () -> new ScientificData(new ArrayList<>(), null));
        assertEquals("Null records", thrown.getMessage());

    }


    @Test
    public void givenInvalidParams_whenConstruct_thenFail() {
        IllegalArgumentException thrown;

        thrown = assertThrows(IllegalArgumentException.class,
                () -> new ScientificData(new ArrayList<>(), new ArrayList<>()));
        assertEquals("Empty header", thrown.getMessage());


        thrown = assertThrows(IllegalArgumentException.class,
                () -> new ScientificData(Arrays.asList("A", "B"), new ArrayList<>()));
        assertEquals("Empty records", thrown.getMessage());

        thrown = assertThrows(IllegalArgumentException.class,
                () -> new ScientificData(Arrays.asList("A", "B", "C"), Collections.singletonList(Arrays.asList("200", "10"))));
        assertEquals("Length of records and header mismatch", thrown.getMessage());

    }


    @Test
    public void givenValidInput_thenHappyScenario() {
        List<String> header = Arrays.asList("id", "name", "mark");

        List<List<String>> records = Arrays.asList(Arrays.asList("200", "Mamoun", "95"), Arrays.asList("400", "Ahmed", "47"));
        ScientificData scientificData = new ScientificData(header, records);

        assertEquals(header, scientificData.getHeader(), "Header mismatch");
        assertEquals(records, scientificData.getRecords(), "Records mismatch");


        List<String> anotherHeader = new ArrayList<>();
        anotherHeader.add("id");
        anotherHeader.add("name");
        anotherHeader.add("mark");

        ScientificData anotherData = new ScientificData(anotherHeader, records);


        assertEquals(anotherData, scientificData);
        assertEquals(anotherData.hashCode(), scientificData.hashCode());

    }

}