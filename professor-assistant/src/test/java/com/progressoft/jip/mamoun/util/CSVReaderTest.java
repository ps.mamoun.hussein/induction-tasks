package com.progressoft.jip.mamoun.util;

import com.progressoft.jip.mamoun.model.ScientificData;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CSVReaderTest {


    @Test
    public void givenNullParams_whenConstruct_thenIllegalArgumentException() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> new CSVReader(null));
        assertEquals("Please provide good buffer reader or use the reader factory", exception.getMessage());
    }

    @Test
    public void givenValidBufferedReaderReader_whenConstruct_thenHappyScenario() throws IOException {
        Path csvPath = Paths.get("src/test/resources/dummy.csv");
        BufferedReader bufferedReader = Files.newBufferedReader(csvPath);
        CSVReader csvReader = new CSVReader(bufferedReader);
        ScientificData scientificData = csvReader.getData();
        assertNotNull(csvReader.getBufferedReader());
        assertNotNull(csvReader);
        assertNotNull(scientificData);

        List<String> expectedHeader = Arrays.asList("Student_id", "class_no", "mark");
        List<List<String>> expectedRecord = Arrays.asList(
                Arrays.asList("202008001", "A", "60"),
                Arrays.asList("202008002", "A", "60"),
                Arrays.asList("202008003", "B", "70")
        );

        assertEquals(scientificData.getHeader(), expectedHeader);
        assertEquals(scientificData.getRecords(), expectedRecord);
        assertEquals(scientificData, new ScientificData(expectedHeader, expectedRecord));
        assertEquals(scientificData, csvReader.getData());

    }
}
