package com.progressoft.jip.mamoun.util;

import com.progressoft.jip.mamoun.model.ScientificData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class CSVWriterTest {

    @Test
    public void givenNullParams_whenConstruct_thenEveryThingWorkingFine() {
        CustomWriter customWriter = new CSVWriter(null, null);
        Assertions.assertNotNull(customWriter);
        List<String> expectedHeader = Arrays.asList("Student_id", "class_no", "mark");
        List<List<String>> expectedRecord = Arrays.asList(
                Arrays.asList("202008001", "A", "60"),
                Arrays.asList("202008002", "A", "60"),
                Arrays.asList("202008003", "B", "70")
        );
        customWriter.writeToFile(new ScientificData(expectedHeader, expectedRecord));
    }




}