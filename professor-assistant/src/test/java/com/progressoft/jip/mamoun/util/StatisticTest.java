package com.progressoft.jip.mamoun.util;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StatisticTest {


    @Test
    public void givenNullData_thenIllegalArgumentException() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> Statistic.mean(null));
        assertEquals(exception.getMessage(), "Data must not be null");
        exception = assertThrows(IllegalArgumentException.class, () -> Statistic.variance(null));
        assertEquals(exception.getMessage(), "Data must not be null");
        exception = assertThrows(IllegalArgumentException.class, () -> Statistic.standardDeviation(null));
        assertEquals(exception.getMessage(), "Data must not be null");
        exception = assertThrows(IllegalArgumentException.class, () -> Statistic.median(null));
        assertEquals(exception.getMessage(), "Data must not be null");
    }

    @Test
    public void givenValidData_thenCalculateAllValue() {
        List<Double> data = Arrays.asList(15.00, 12.00, 17.00, 20.00, 30.00, 31.00, 43.00, 44.00, 54.00);
        assertEquals(Statistic.mean(data).doubleValue(), 29.56);
        assertEquals(Statistic.variance(data).doubleValue(), 195.36);
        assertEquals(Statistic.standardDeviation(data).doubleValue(), 13.98);
        assertEquals(Statistic.median(data).doubleValue(), 30.00);

    }
}