package org.example;

import java.util.HashMap;
import java.util.Scanner;
import java.util.Stack;

import static org.example.utils.CharType.*;
import static org.example.utils.OperationsUtil.*;

public class Main {
    public static void main(String[] args) {
        // TODO, throw custom exception instead of printing to the user (DONE)
        if (args.length == 0)
            throw new IllegalArgumentException("Please provide equation to solve");
        else {
            Stack<Double> operands = new Stack<>();
            Stack<String> operations = new Stack<>();
            Scanner scanner = new Scanner(System.in);
            HashMap<String, Double> variableMap = new HashMap<>();
            for (String character : fixUserInput(String.join("", args))) {
                if (getType(character) == NUMBER) {
                    operands.push(Double.parseDouble(character));
                } else if (getType(character) == VARIABLE) {
                    if (!variableMap.containsKey(character)) {
                        System.out.println("Please provide value for " + character);
                        double userInput = scanner.nextDouble();
                        variableMap.put(character, userInput);
                    }
                    operands.push(variableMap.get(character));
                } else {
                    addOperations(character, operations, operands);
                }
            }
            scanner.close();
            while (!operations.isEmpty()) {
                String op = operations.pop();
                operates(operands, op);
            }
            System.out.println("The final Answer is = " + operands.peek());
        }
    }


}