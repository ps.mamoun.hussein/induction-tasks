package org.example.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import static org.example.utils.CharType.*;

public class OperationsUtil {


    public static List<String> fixUserInput(String userInput) {
        List<String> fixed = new ArrayList<>();
        StringBuilder numberFormat = new StringBuilder();
        userInput = userInput.replaceAll(" ", ""); // replace all does not work well on Windows
        for (int i = 0; i < userInput.length(); i++) {
            String character = String.valueOf(userInput.charAt(i));
            if (getType(character) == NUMBER) {
                numberFormat.append(character);
                continue;
            }
            if (numberFormat.length() != 0) {
                fixed.add(numberFormat.toString());
                numberFormat.setLength(0);
            }
            if (fixed.size() > 0 && (getType(fixed.get(fixed.size() - 1)) == NUMBER || getType(fixed.get(fixed.size() - 1)) == VARIABLE)) {

                if (getType(character) == VARIABLE || character.equals("(")) {
                    fixed.add("*");
                }
            }

            if (character.equals("-") && getType(String.valueOf(userInput.charAt(i + 1))) == NUMBER) {
                numberFormat.append("-");
                fixed.add("+");
                continue;
            }

            fixed.add(character);
        }
        if (numberFormat.length() != 0) {
            fixed.add(numberFormat.toString());
        }
        return fixed;
    }

    public static CharType getType(String unknownString) {
        //
        if (Character.isAlphabetic(unknownString.charAt(unknownString.length() - 1))) {
            return CharType.VARIABLE;
        } else if (Character.isDigit(unknownString.charAt(unknownString.length() - 1))) {
            return NUMBER;
        }
        // TODO, you can remove the last else and keep it as return CharType.OPERATIONS (DONE)
        return CharType.OPERATIONS;

    }

    public static void operates(Stack<Double> operands, String op) {
        double firstValue = operands.pop();
        switch (op) {
            case "+":
                operands.push(operands.pop() + firstValue);
                break;
            case "-":
                operands.push(operands.pop() - firstValue);
                break;
            case "*":
                operands.push(operands.pop() * firstValue);
                break;
            case "/":
                operands.push(operands.pop() / firstValue);
                break;
            case "^":
                operands.push(Math.pow(operands.pop(), firstValue));
                break;
        }
    }

    public static void addOperations(String op, Stack<String> operations, Stack<Double> operands) {
        switch (op) {
            case "(":
            case "^":
                operations.push(op);
                break;
            case "*":
            case "/":
                if (!operations.isEmpty() && operations.peek().equals("^")) {
                    operates(operands, op);
                    operations.pop();
                    addOperations(op, operations, operands);
                } else operations.push(op);
                break;
            case "+":
            case "-":
                if (!operations.isEmpty() && (operations.peek().equals("*") || operations.peek().equals("/"))) {
                    operates(operands, op);
                    operations.pop();
                    addOperations(op, operations, operands);
                } else operations.push(op);
                break;
            case ")":
                String pop = operations.pop();
                if (!pop.equals("(")) {
                    operates(operands, pop);
                    addOperations(op, operations, operands);
                }
                break;
        }
    }
}
