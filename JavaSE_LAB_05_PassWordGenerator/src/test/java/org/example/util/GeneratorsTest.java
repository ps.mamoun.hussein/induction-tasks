package org.example.util;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class GeneratorsTest {

    Generators generators = new Generators();
    private final String UPPER_CASE_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private final String SYMBOL = "_$#%";
    private final String NUMBERS = "0123456789";
    private static final int UPPER_NUMBER = 10;
    private final int ACTUAL_NUMBER_COUNT = 4;
    private final int ACTUAL_UPPER_CASE_COUNT = 2;
    private final int ACTUAL_SYMBOL_COUNT = 2;

    @Test
    public void randomPasswordUpperCaseLetter() {
        for (int i = 0; i < UPPER_CASE_LETTERS.length(); i++)
            assertTrue(UPPER_CASE_LETTERS.contains(generators.randomPasswordUpperCaseLetter()), "Expected to generate Upper case letter");
    }

    @Test
    public void randomPasswordSymbol() {
        for (int i = 0; i < SYMBOL.length(); i++)
            assertTrue(SYMBOL.contains(generators.randomPasswordSymbol()), "Expected to generate Symbol");
    }

    @Test
    public void randomPassWordNumber() {
        for (int i = 0; i < UPPER_NUMBER; i++) {
            try {
                int number = Integer.parseInt(generators.randomPassWordNumber());
                assertTrue(number >= 0 && number < UPPER_NUMBER, "The number should be between 0-9 inclusive");
            } catch (NumberFormatException e) {
                Assertions.fail("Expected random number but found" + e);
            }
        }


    }

    @RepeatedTest(1000)
    public void generateRandomPassword() {
        String randomPassword = generators.generateRandomPassword(ACTUAL_NUMBER_COUNT, ACTUAL_UPPER_CASE_COUNT, ACTUAL_SYMBOL_COUNT);
        final int ACTUAL_PASSWORD_LENGTH = ACTUAL_NUMBER_COUNT + ACTUAL_UPPER_CASE_COUNT + ACTUAL_SYMBOL_COUNT;
        int numberCount = 0;
        int upperCaseCount = 0;
        int symbolCount = 0;
        assertEquals(randomPassword.length(), ACTUAL_PASSWORD_LENGTH, "Expected the password to be 8 char length");

        for (int i = 0; i < randomPassword.length(); i++) {
            String singleChar = String.valueOf(randomPassword.charAt(i));
            if (UPPER_CASE_LETTERS.contains(singleChar)) {
                upperCaseCount++;
            } else if (SYMBOL.contains(singleChar)) {
                symbolCount++;
            } else if (NUMBERS.contains(singleChar)) {
                numberCount++;
            }
        }
        assertEquals(symbolCount, ACTUAL_SYMBOL_COUNT, "Expected the symbol to be " + ACTUAL_SYMBOL_COUNT + "But found at " + symbolCount);
        assertEquals(upperCaseCount, ACTUAL_UPPER_CASE_COUNT, "Expected the upper case to be " + ACTUAL_UPPER_CASE_COUNT + "But found at " + upperCaseCount);
        assertEquals(numberCount, ACTUAL_NUMBER_COUNT, "Expected the number to be " + ACTUAL_NUMBER_COUNT + "But found at " + numberCount);
        String randomPassword2 = generators.generateRandomPassword(ACTUAL_NUMBER_COUNT, ACTUAL_UPPER_CASE_COUNT, ACTUAL_SYMBOL_COUNT);
        assertNotEquals(randomPassword, randomPassword2, "Two password should not be the same");


    }

    @Test
    public void generateRandomItem() {
        for (PasswordItem item : PasswordItem.values()) {
            assertNotSame(generators.generateRandomItem(item), item, "Expected to exclude" + item);
        }
        assertNotNull(generators.generateRandomItem(null), "Expected to not return null");
    }

    @Test
    public void generateRandomCharBasedOnItem() {
        for (PasswordItem item : PasswordItem.values()) {
            String charBasedOnItem = generators.generateRandomCharBasedOnItem(item);
            assertEquals(charBasedOnItem.length(), 1, "Expected the function to return one length single item");

            switch (item) {
                case UPPER_CASE:
                    assertTrue(UPPER_CASE_LETTERS.contains(charBasedOnItem), "Expected to be " + item);
                    break;
                case SYMBOLS:
                    assertTrue(SYMBOL.contains(charBasedOnItem), "Expected to be " + item);
                    break;
                case NUMBER:
                    assertTrue(NUMBERS.contains(charBasedOnItem), "Expected to be " + item);
                    break;
            }
        }
    }
}