package org.example;

import org.example.util.Generators;

public class Main {
    public static void main(String[] args) {
        Generators generators = new Generators();
        // TODO, add it inside a variable (DONE)
        String generatedPassword = generators.generateRandomPassword(
                Generators.ACTUAL_NUMBER_COUNT,
                Generators.ACTUAL_UPPER_CASE_COUNT,
                Generators.ACTUAL_SYMBOL_COUNT);
        System.out.println(generatedPassword);
    }
}