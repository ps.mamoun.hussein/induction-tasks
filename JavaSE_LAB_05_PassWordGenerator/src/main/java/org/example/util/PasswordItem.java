package org.example.util;

public enum PasswordItem {
    NUMBER,
    UPPER_CASE,
    SYMBOLS
}
