package org.example.util;

import java.util.Random;

import static org.example.util.PasswordItem.*;

public class Generators {

    private final String UPPER_CASE_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private final String SYMBOL = "_$#%";
    private final int UPPER_NUMBER = 10;
    private final Random random = new Random();
    public static final int ACTUAL_NUMBER_COUNT = 4;
    public static final int ACTUAL_UPPER_CASE_COUNT = 2;
    public static final int ACTUAL_SYMBOL_COUNT = 2;


    // the max number is not included if the max is 5 the range 0-4
    // TODO Optional, Read about SecureRandom (DONE)
    private int randomNumber(int maxNumber) {
        return random.nextInt(maxNumber);
    }

    public String randomPasswordUpperCaseLetter() {
        return String.valueOf(UPPER_CASE_LETTERS.charAt(randomNumber(UPPER_CASE_LETTERS.length())));
    }

    public String randomPasswordSymbol() {
        return String.valueOf(SYMBOL.charAt(randomNumber(SYMBOL.length())));
    }

    public String randomPassWordNumber() {
        return String.valueOf(randomNumber(UPPER_NUMBER));
    }

    public String generateRandomPassword(int numberCount, int upperCaseCount, int symbolCount) {
        StringBuilder passwordBuilder = new StringBuilder();
        int passwordLength = numberCount + upperCaseCount + symbolCount;
        int lengthOfNumbers = 0;
        int lengthOfUpperCase = 0;
        int lengthOfSymbols = 0;
        PasswordItem item = generateRandomItem(null);

        while (passwordBuilder.length() < passwordLength) {
            if (item == NUMBER && lengthOfNumbers < numberCount) {
                passwordBuilder.append(generateRandomCharBasedOnItem(item));
                lengthOfNumbers++;
            } else if (item == UPPER_CASE && lengthOfUpperCase < upperCaseCount) {
                passwordBuilder.append(generateRandomCharBasedOnItem(item));
                lengthOfUpperCase++;
            } else if (item == SYMBOLS && lengthOfSymbols < symbolCount) {
                passwordBuilder.append(generateRandomCharBasedOnItem(item));
                lengthOfSymbols++;
            }
            item = generateRandomItem(item);
        }
        return passwordBuilder.toString();
    }


    public PasswordItem generateRandomItem(PasswordItem lastItem) {
        PasswordItem item = PasswordItem.values()[randomNumber(PasswordItem.values().length)];
        while (item == lastItem) {
            item = PasswordItem.values()[randomNumber(PasswordItem.values().length)];
        }
        return item;
    }


    public String generateRandomCharBasedOnItem(PasswordItem item) {
        String passWordChar = "";
        if (item == NUMBER) {
            passWordChar = randomPassWordNumber();
        } else if (item == SYMBOLS) {
            passWordChar = randomPasswordSymbol();
        } else if (item == UPPER_CASE) {
            passWordChar = randomPasswordUpperCaseLetter();
        }
        return passWordChar;
    }


}