# Reconciliation

This application is for the induction tasks phase #2 made by **Mamoun Hussein**, and the requirement can be found
at [This link](https://gitlab.com/ps.ahmad.saleh/induction-reconciliation-task)

## Idea

This application idea is to compare two files that have been written using two different systems for Transaction using
unique identifier using 'transaction id' and generate three files with the result of 'matching, misMatching and missing'
transactions.

### Configure

To configure the app please take look at each module below if you want to add some extra implementations and if you
don't want you can skip this.

- Default IO Configuration :
    - Reader (CSV, JSON)
    - Writer (CSV)

To change this configuration click [here](./reconciliation-IO/src/main/resources/supported-extensions.properties) to
change it from the propirtes and don't forget to provide an implementation for the newly supported extension

- Default Currency Configuration:
    - Currency Decimal is provided by [this file](./reconciliation-repo/src/main/resources/currencyCode.csv) as it's
      been copied from wikipedia if you want to adjust the decimal point and currency code naming feel free to change as
      the way you like.

#### Run the application

To start the application enter **reconciliation-console-app** module and click on the main function in the App class.




