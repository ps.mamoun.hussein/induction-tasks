package com.progressoft.jip.mamoun;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class CsvTest {

    @Test
    public void givenValidParam_whenConstruct_thenHappyScenario() {
        Csv csv = new Csv("validID", "validDesc", "50.25", "USD", "Allownce", "1998-04-06", "D");
        Csv csv2 = new Csv("validID", "validDesc", "50.25000", "USD", "Allownce", "1998-04-06", "D");
        assertEquals(csv, csv2);
        assertEquals(csv.hashCode(), csv2.hashCode());
        assertEquals(csv.getTransactionId(), "validID");
        assertNotNull(csv.getValueDate());
        assertEquals(csv2.getAmount().compareTo(new BigDecimal("50.25")), 0);
    }

}