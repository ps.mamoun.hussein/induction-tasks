package com.progressoft.jip.mamoun;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CurrencyTest {


    @Test
    public void givenValidParams_whenConstruct_thenHappyScansion() {
        Currency currency = new Currency("USD", 2, 2, "Jordan", "Jordan");
        Currency currency2 = new Currency("USD", 2, 2, "Jordan", "Jordan");
        assertEquals(currency2, currency);
        assertEquals(currency2.hashCode(), currency.hashCode());

    }

}