package com.progressoft.jip.mamoun;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class JsonTest {

    @Test
    public void givenValidParam_whenConstruct_thenHappyScenario() {
        Json json = new Json("06/04/1998", "validID", "50.25", "allowance", "USD");

        Json json2 = new Json("06/04/1998", "validID", "50.2500", "allowance", "USD");

        assertEquals(json, json2);
        assertEquals(json.hashCode(), json2.hashCode());
        assertEquals(json.getTransactionId(), "validID");
        assertNotNull(json.getValueDate());
        assertEquals(json2.getAmount().compareTo(new BigDecimal("50.25")), 0);
    }

}