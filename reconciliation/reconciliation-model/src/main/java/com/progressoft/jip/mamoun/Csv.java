package com.progressoft.jip.mamoun;

import com.opencsv.bean.CsvBindByPosition;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


// TODO, no tests for the model? (DONE)
public class Csv implements GeneralModel {

    public static final String DATE_PATTERN = "yyyy-MM-dd";
    @CsvBindByPosition(position = 0)
    private String transUniqueId;
    @CsvBindByPosition(position = 1)
    private String transDescription;
    @CsvBindByPosition(position = 2)
    private String amount;
    @CsvBindByPosition(position = 3)
    private String currency;
    @CsvBindByPosition(position = 4)
    private String purpose;
    @CsvBindByPosition(position = 5)
    private String valueDate;
    @CsvBindByPosition(position = 6)
    private String transType;

    public Csv() {
    }

    public Csv(String transUniqueId, String transDescription, String amount, String currency, String purpose, String valueDate, String transType) {
        this.transUniqueId = transUniqueId;
        this.transDescription = transDescription;
        this.amount = amount;
        this.currency = currency;
        this.purpose = purpose;
        this.valueDate = valueDate;
        this.transType = transType;
    }

    @Override
    public String getTransactionId() {
        return transUniqueId.trim();
    }

    @Override
    public BigDecimal getAmount() {
        return new BigDecimal(amount.trim());
    }

    @Override
    public String getCurrencyCode() {
        return currency.trim();
    }


    public String getTransDescription() {
        return transDescription.trim();
    }

    public String getPurpose() {
        return purpose.trim();
    }

    public String getTransType() {
        return transType.trim();
    }

    @Override
    public Date getValueDate() {
        try {
            return new SimpleDateFormat(DATE_PATTERN).parse(valueDate);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public String toString() {
        return "CSVFile{" +
                "transUniqueId='" + getTransactionId() + '\'' +
                ", transDescription='" + getTransDescription() + '\'' +
                ", amount='" + getAmount() + '\'' +
                ", currency='" + getCurrencyCode() + '\'' +
                ", purpose='" + getPurpose() + '\'' +
                ", valueDate='" + getValueDate() + '\'' +
                ", transType='" + getTransType() + '\'' +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Csv)) return false;

        Csv csv = (Csv) o;

        if (!getTransactionId().equals(csv.getTransactionId())) return false;
        if (getAmount().compareTo(csv.getAmount()) != 0) return false;
        if (!getCurrencyCode().equals(csv.getCurrencyCode())) return false;
        return getValueDate().equals(csv.getValueDate());
    }

    @Override
    public int hashCode() {
        int result = getTransactionId().hashCode();
        result = 31 * result + getAmount().intValue();
        result = 31 * result + getCurrencyCode().hashCode();
        result = 31 * result + getValueDate().hashCode();
        return result;
    }
}
