package com.progressoft.jip.mamoun;

import com.opencsv.bean.CsvBindByPosition;

// TODO, no tests for the model? (DONE)
public class Currency {

    @CsvBindByPosition(position = 0)
    private String currencyCode;

    @CsvBindByPosition(position = 1)
    private int num;

    @CsvBindByPosition(position = 2)
    private int decimalPoint;

    @CsvBindByPosition(position = 3)
    private String currencyName;

    @CsvBindByPosition(position = 4)
    private String location;

    public Currency() {
    }

    public Currency(String currencyCode, int num, int decimalPoint, String currencyName, String location) {
        this.currencyCode = currencyCode;
        this.num = num;
        this.decimalPoint = decimalPoint;
        this.currencyName = currencyName;
        this.location = location;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public int getNum() {
        return num;
    }

    public int getDecimalPoint() {
        return decimalPoint;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public String getLocation() {
        return location;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Currency)) return false;

        Currency currency1 = (Currency) o;

        if (getNum() != currency1.getNum()) return false;
        if (getDecimalPoint() != currency1.getDecimalPoint()) return false;
        if (getCurrencyCode() != null ? !getCurrencyCode().equals(currency1.getCurrencyCode()) : currency1.getCurrencyCode() != null)
            return false;
        if (getCurrencyName() != null ? !getCurrencyName().equals(currency1.getCurrencyName()) : currency1.getCurrencyName() != null)
            return false;
        return getLocation() != null ? getLocation().equals(currency1.getLocation()) : currency1.getLocation() == null;
    }

    @Override
    public int hashCode() {
        int result = getCurrencyCode() != null ? getCurrencyCode().hashCode() : 0;
        result = 31 * result + getNum();
        result = 31 * result + getDecimalPoint();
        result = 31 * result + (getCurrencyName() != null ? getCurrencyName().hashCode() : 0);
        result = 31 * result + (getLocation() != null ? getLocation().hashCode() : 0);
        return result;
    }
}
