package com.progressoft.jip.mamoun;

import java.math.BigDecimal;
import java.util.Date;

/**
 * This interface is the general form of data where every model
 * must inherit it for order for the application to be generic
 */
public interface GeneralModel {


    String getTransactionId();


    BigDecimal getAmount();


    String getCurrencyCode();


    Date getValueDate();
}
