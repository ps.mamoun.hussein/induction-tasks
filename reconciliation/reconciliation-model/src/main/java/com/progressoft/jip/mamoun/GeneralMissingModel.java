package com.progressoft.jip.mamoun;

public interface GeneralMissingModel {

    GeneralModel getGeneralModel();

    String getFileSource();
}
