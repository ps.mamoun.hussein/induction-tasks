package com.progressoft.jip.mamoun;


// TODO, no tests for the getters?
// They are normal getter no test needed (DONE)


public class MissingModel implements GeneralMissingModel {
    private final GeneralModel generalModel;

    private final String fileSource;

    public MissingModel(GeneralModel generalModel, String fileSource) {
        this.generalModel = generalModel;
        this.fileSource = fileSource;
    }

    public GeneralModel getGeneralModel() {
        return generalModel;
    }

    public String getFileSource() {
        return fileSource;
    }
}
