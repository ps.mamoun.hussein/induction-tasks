package com.progressoft.jip.mamoun;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

// TODO, no tests for the model? (DONE)
public class Json implements GeneralModel {

    private static final String DATE_PATTERN = "dd/MM/yyyy";
    @SerializedName("reference")
    private String reference;
    @SerializedName("amount")
    private String amount;
    @SerializedName("currencyCode")
    private String currencyCode;
    @SerializedName("date")
    private String date;
    @SerializedName("purpose")
    private String purpose;

    public Json() {
    }

    public Json(String date, String reference, String amount, String purpose, String currencyCode) {
        this.date = date;
        this.reference = reference;
        this.amount = amount;
        this.purpose = purpose;
        this.currencyCode = currencyCode;
    }

    public String getReference() {
        return reference.trim();
    }

    public String getPurpose() {
        if (purpose != null)
            return purpose.trim();
        return "";
    }

    @Override
    public String getTransactionId() {
        return reference.trim();
    }

    @Override
    public BigDecimal getAmount() {
        return new BigDecimal(amount.trim());
    }

    @Override
    public String getCurrencyCode() {
        return currencyCode.trim();
    }

    @Override
    public Date getValueDate() {
        try {
            return new SimpleDateFormat(DATE_PATTERN).parse(date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @Override
    public String toString() {
        return
                "ResponseItem{" +
                        "date = '" + getValueDate() + '\'' +
                        ",reference = '" + getReference() + '\'' +
                        ",amount = '" + getAmount() + '\'' +
                        ",purpose = '" + getPurpose() + '\'' +
                        ",currencyCode = '" + getCurrencyCode() + '\'' +
                        "}";
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Json)) return false;

        Json reconciliationJsonDataFormat = (Json) o;

        if (!getValueDate().equals(reconciliationJsonDataFormat.getValueDate())) return false;
        if (!getReference().equals(reconciliationJsonDataFormat.getReference())) return false;
        if (getAmount().compareTo(reconciliationJsonDataFormat.getAmount()) != 0) return false;
        return getCurrencyCode().equals(reconciliationJsonDataFormat.getCurrencyCode());
    }

    @Override
    public int hashCode() {
        int result = getValueDate().hashCode();
        result = 31 * result + getReference().hashCode();
        result = 31 * result + getAmount().intValue();
        result = 31 * result + getCurrencyCode().hashCode();
        return result;
    }


}
