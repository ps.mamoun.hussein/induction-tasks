package com.progressoft.jip.mamoun.writer.factory;


import com.progressoft.jip.mamoun.util.FactoryUtility;
import com.progressoft.jip.mamoun.writer.CsvWriter;
import com.progressoft.jip.mamoun.writer.JsonWriter;
import com.progressoft.jip.mamoun.writer.ReconciliationWriter;

import java.util.List;

public abstract class ReconciliationWriterFactory {

    public static final List<String> supportedExtensions;


    static {

        //  TODO duplicated code from ReconciliationReaderFactory, extract them to another class, FactoryUtility as an example (DONE)

        supportedExtensions = FactoryUtility.getSupportedExtension("supported_write_extensions");

    }

    public static ReconciliationWriter getWriter(String outExtension) {
        if (!supportedExtensions.contains(outExtension))
            throw new IllegalArgumentException("Unsupported extension");
        switch (outExtension.trim().toLowerCase()) {
            case "csv":
                return new CsvWriter();
            case "json":
                // TODO implement json writer
                return new JsonWriter();
            default:
                throw new IllegalArgumentException("Your 'supported-extensions.properties' file contains " + outExtension + " which is not implemented yet please provide implementation for this reader in order to support it");
        }

    }
}
