package com.progressoft.jip.mamoun.reader;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.progressoft.jip.mamoun.GeneralModel;
import com.progressoft.jip.mamoun.Json;

import java.io.Reader;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class JsonReader implements ReconciliationReader {

    private final List<GeneralModel> generalDataFormat;


    public JsonReader(Reader reader) throws JsonSyntaxException, JsonIOException {
        if (reader == null)
            throw new IllegalArgumentException("Reader must not be null");

        Gson gson = new Gson();
        generalDataFormat = Arrays.asList(gson.fromJson(reader, Json[].class));

    }

    @Override
    public List<GeneralModel> getReconciliationData() {
        return Collections.unmodifiableList(generalDataFormat);
    }
}
