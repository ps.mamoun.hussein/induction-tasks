package com.progressoft.jip.mamoun.reader;

import com.opencsv.bean.CsvToBeanBuilder;
import com.progressoft.jip.mamoun.Csv;
import com.progressoft.jip.mamoun.GeneralModel;


import java.io.Reader;
import java.util.Collections;
import java.util.List;

public class CsvReader implements ReconciliationReader {

    private final List<GeneralModel> generalDataFormatList;

    public CsvReader(Reader reader) throws IllegalStateException {
        if (reader == null)
            throw new IllegalArgumentException("Reader must not be null");
        generalDataFormatList = new CsvToBeanBuilder<GeneralModel>(reader)
                .withSkipLines(1)
                .withType(Csv.class)
                .withIgnoreLeadingWhiteSpace(true)
                .build()
                .parse();
    }

    @Override
    public List<GeneralModel> getReconciliationData() {
        return Collections.unmodifiableList(generalDataFormatList);
    }

}
