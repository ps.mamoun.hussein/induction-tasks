package com.progressoft.jip.mamoun.reader;


import com.progressoft.jip.mamoun.GeneralModel;

import java.util.List;

public interface ReconciliationReader {

    List<GeneralModel> getReconciliationData();

}
