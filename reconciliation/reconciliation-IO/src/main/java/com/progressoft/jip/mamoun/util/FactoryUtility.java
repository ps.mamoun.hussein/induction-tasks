package com.progressoft.jip.mamoun.util;

import com.progressoft.jip.mamoun.reader.factory.ReconciliationReaderFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public abstract class FactoryUtility {
    private static final String FILE_NAME = "supported-extensions.properties";

    public static List<String> getSupportedExtension(String propertiesName) {
        try {
            Properties properties = new Properties();
            properties.load(ReconciliationReaderFactory.class.getClassLoader().getResourceAsStream(FILE_NAME));
            return Arrays.asList(properties.get(propertiesName).toString().replaceAll(" ", "").toLowerCase().split(","));
        } catch (IOException e) {
            throw new RuntimeException("Please provide supported " + FILE_NAME + "file in resources");
        }
    }
}
