package com.progressoft.jip.mamoun.writer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.progressoft.jip.mamoun.GeneralMissingModel;
import com.progressoft.jip.mamoun.GeneralModel;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;

public class JsonWriter implements ReconciliationWriter {

    private final Path FOLDER_PATH = Paths.get("out-folder", String.valueOf(LocalDateTime.now()));

    private final Gson gson = new GsonBuilder().setPrettyPrinting().create();


    public JsonWriter() {
        try {
            if (Files.notExists(FOLDER_PATH.getParent())) {
                Files.createDirectory(FOLDER_PATH.getParent());
            }
            Files.createDirectory(FOLDER_PATH);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void writeMatchingFile(List<GeneralModel> matchingData) {
        Path filePath = Paths.get(String.valueOf(FOLDER_PATH), "matching.json");
        try {
            BufferedWriter writer = Files.newBufferedWriter(filePath);
            gson.toJson(matchingData, writer);
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException("Could not create matching json file");
        }
    }

    @Override
    public void writeMisMatchingFile(List<GeneralMissingModel> misMatchData) {
        Path filePath = Paths.get(String.valueOf(FOLDER_PATH), "mismatching.json");
        jsonWrite(misMatchData, filePath);
    }

    private void jsonWrite(List<GeneralMissingModel> misMatchData, Path filePath) {
        try {
            BufferedWriter writer = Files.newBufferedWriter(filePath);
            gson.toJson(misMatchData, writer);
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException("Could not create missing json file");
        }
    }

    @Override
    public void writeMissingFile(List<GeneralMissingModel> missingData) {
        Path filePath = Paths.get(String.valueOf(FOLDER_PATH), "missing.json");
        jsonWrite(missingData, filePath);
    }

    @Override
    public Path getFOLDER_PATH() {
        return FOLDER_PATH;
    }
}
