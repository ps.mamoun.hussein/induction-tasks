package com.progressoft.jip.mamoun.reader.factory;

import com.progressoft.jip.mamoun.reader.CsvReader;
import com.progressoft.jip.mamoun.reader.JsonReader;
import com.progressoft.jip.mamoun.reader.ReconciliationReader;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static com.progressoft.jip.mamoun.util.FactoryUtility.getSupportedExtension;

public abstract class ReconciliationReaderFactory {
    public static final List<String> supportedExtensions;


    static {
        // TODO, you can create a class and call it ReconciliationConstant, you can save system global variables like the property file nam
        //  also duplicated code from ReconciliationWriterFactory, extract them to another class, FactoryUtility as an example (DONE)

        supportedExtensions = getSupportedExtension("supported_read_extensions");

    }

    public static ReconciliationReader getReader(Path path) throws IllegalArgumentException {
        if (path == null) throw new IllegalArgumentException("Path must not be null");

        String extension = getExtension(path).toLowerCase();
        if (!supportedExtensions.contains(extension)) throw new IllegalArgumentException("Unsupported extension");
        try (Reader reader = Files.newBufferedReader(path)) {
            switch (extension) {
                case "csv":
                    return new CsvReader(reader);
                case "json":
                    return new JsonReader(reader);
                default:
                    throw new IllegalArgumentException("Your 'supported-extensions.properties' file contains " + extension + " which is not implemented yet please provide implementation for this reader in order to support it");
            }

        } catch (IOException e) {
            throw new IllegalArgumentException("File not found");
        }
    }


    private static String getExtension(Path path) throws IllegalArgumentException {
        String fileName = path.getFileName().toString();
        int dotIndex = fileName.lastIndexOf(".");
        if (dotIndex == -1) throw new IllegalArgumentException("Unknown file format");
        return fileName.substring(dotIndex + 1);

    }


}
