package com.progressoft.jip.mamoun.writer;

import com.opencsv.CSVWriter;
import com.progressoft.jip.mamoun.GeneralMissingModel;
import com.progressoft.jip.mamoun.GeneralModel;
import com.progressoft.jip.mamoun.dao.CurrencyDao;
import com.progressoft.jip.mamoun.dao.imp.CurrencyDaoImp;
import com.progressoft.jip.mamoun.date.CustomDateParser;

import java.io.IOException;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;

public class CsvWriter implements ReconciliationWriter {

    private final CurrencyDao currencyDao = new CurrencyDaoImp();

    private final Path FOLDER_PATH = Paths.get("out-folder", String.valueOf(LocalDateTime.now()));

    public CsvWriter() {
        try {
            if (Files.notExists(FOLDER_PATH.getParent())) {
                Files.createDirectory(FOLDER_PATH.getParent());
            }
            Files.createDirectory(FOLDER_PATH);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void writeMatchingFile(List<GeneralModel> matchingList) {
        Path filePath = Paths.get(String.valueOf(FOLDER_PATH), "matching.csv");
        try {
            final CSVWriter openCsvWriter = new CSVWriter(Files.newBufferedWriter(filePath));
            openCsvWriter.writeNext(
                    new String[]{
                            "transaction id",
                            "amount",
                            "currency code",
                            "value date"
                    });
            for (GeneralModel matchingData : matchingList) {
                String currencyCode = matchingData.getCurrencyCode();
                openCsvWriter.writeNext(
                        new String[]{
                                matchingData.getTransactionId(),
                                getFixedAmount(matchingData),
                                currencyCode,
                                CustomDateParser.parse(matchingData.getValueDate())
                        });
            }
            openCsvWriter.close();
        } catch (IOException e) {
            throw new IllegalArgumentException("Could not create file with name of " + filePath);
        }

    }

    private String getFixedAmount(GeneralModel matchingData) {
        return String.valueOf(matchingData.getAmount().setScale(currencyDao.getDecimal(matchingData.getCurrencyCode()), RoundingMode.HALF_EVEN));
    }

    @Override
    public void writeMisMatchingFile(List<GeneralMissingModel> misMatchingList) {
        Path filePath = Paths.get(String.valueOf(FOLDER_PATH), "mismatching.csv");
        writeFile(misMatchingList, filePath);
    }

    private void writeFile(List<GeneralMissingModel> misMatchingList, Path filePath) {
        try {
            final CSVWriter openCsvWriter = new CSVWriter(Files.newBufferedWriter(filePath));
            openCsvWriter.writeNext(
                    new String[]{
                            "found in file",
                            "transaction id",
                            "amount",
                            "currency code",
                            "value date"
                    });
            for (GeneralMissingModel misMatchData : misMatchingList) {
                GeneralModel matchingData = misMatchData.getGeneralModel();
                openCsvWriter.writeNext(
                        new String[]{
                                misMatchData.getFileSource(),
                                matchingData.getTransactionId(),
                                getFixedAmount(matchingData),
                                matchingData.getCurrencyCode(),
                                CustomDateParser.parse(matchingData.getValueDate())
                        });
            }
            openCsvWriter.close();
        } catch (IOException e) {
            throw new IllegalArgumentException("Could not create file with name of " + filePath);
        }
    }

    @Override
    public void writeMissingFile(List<GeneralMissingModel> missingList) {
        Path filePath = Paths.get(String.valueOf(FOLDER_PATH), "missing.csv");
        writeFile(missingList, filePath);
    }

    @Override
    public Path getFOLDER_PATH() {
        return FOLDER_PATH;
    }

}
