package com.progressoft.jip.mamoun.writer;

import com.progressoft.jip.mamoun.GeneralMissingModel;
import com.progressoft.jip.mamoun.GeneralModel;

import java.nio.file.Path;
import java.util.List;

public interface ReconciliationWriter {


    void writeMatchingFile(List<GeneralModel> matchingData);

    void writeMisMatchingFile(List<GeneralMissingModel> misMatchData);

    void writeMissingFile(List<GeneralMissingModel> missingData);

    Path getFOLDER_PATH();
}
