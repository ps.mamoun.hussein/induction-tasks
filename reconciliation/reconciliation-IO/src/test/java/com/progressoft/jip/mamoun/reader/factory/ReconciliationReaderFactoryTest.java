package com.progressoft.jip.mamoun.reader.factory;


import com.progressoft.jip.mamoun.reader.CsvReader;
import com.progressoft.jip.mamoun.reader.JsonReader;
import com.progressoft.jip.mamoun.reader.ReconciliationReader;
import com.progressoft.jip.mamoun.reader.factory.ReconciliationReaderFactory;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

class ReconciliationReaderFactoryTest {


    @Test
    public void givenNullPath_whenCallingGetReader_thenIllegalArgumentsException() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> ReconciliationReaderFactory.getReader(null));
        assertEquals("Path must not be null", exception.getMessage());
    }

    @Test
    public void givenPathWithoutExtension_whenCallingGetReader_thenIllegalArgumentException() {
        Path path = Paths.get("noFileFormatExtension");
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> ReconciliationReaderFactory.getReader(path));
        assertEquals(exception.getMessage(), "Unknown file format");
    }


    @Test
    public void givenPathWithNotSupportedExtension_whenCallingGetReader_thenIllegalArgumentException() {
        Path path = Paths.get("validFile.invalidFormat");
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> ReconciliationReaderFactory.getReader(path));
        assertEquals(exception.getMessage(), "Unsupported extension");
    }


    @Test
    public void givenInvalidFilePathWithValidExtension_whenCallingGetReader_thenIllegalArgumentException() {
        Path path = Paths.get("invalidPath.csv");
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> ReconciliationReaderFactory.getReader(path));
        assertEquals(exception.getMessage(), "File not found");
    }

    @Test
    public void givenValidFilePathWithValidExtensionButNotImplemented_whenCallingGetReader_thenIllegalArgumentException() {
        Path path = Paths.get("src/test/resources/dummy.xml");
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> ReconciliationReaderFactory.getReader(path));
        assertEquals(exception.getMessage(), "Your 'supported-extensions.properties' file contains xml which is not implemented yet please provide implementation for this reader in order to support it");
    }


    @Test
    public void givenValidFile_whenCallingGetReader_thenReturnTheCrossbreedingReaderType() {
        Path csvPath = Paths.get("src/test/resources/bank-transactions.csv");
        Path jsonPath = Paths.get("src/test/resources/online-banking-transactions.json");
        ReconciliationReader csvReader = ReconciliationReaderFactory.getReader(csvPath);
        ReconciliationReader jsonReader = ReconciliationReaderFactory.getReader(jsonPath);
        assertTrue(csvReader instanceof CsvReader);
        assertTrue(jsonReader instanceof JsonReader);
    }

}