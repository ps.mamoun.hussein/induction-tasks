package com.progressoft.jip.mamoun.reader;


import com.progressoft.jip.mamoun.Csv;
import com.progressoft.jip.mamoun.GeneralModel;
import com.progressoft.jip.mamoun.reader.CsvReader;
import com.progressoft.jip.mamoun.reader.ReconciliationReader;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CsvReaderTest {

    @Test
    public void givenNullParam_whenConstruct_thenIllegalArgumentException() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> new CsvReader(null));
        assertEquals(exception.getMessage(), "Reader must not be null");
    }


    @Test
    public void givenValidCsvReader_whenConstruct_thenHappyScenarioWithUnmodifiableList() throws IOException {
        Path path = Paths.get("src/test/resources/bank-transactions.csv");
        Reader reader = Files.newBufferedReader(path);
        ReconciliationReader reconciliationReader = new CsvReader(reader);
        List<GeneralModel> reconciliationData = reconciliationReader.getReconciliationData();
        assertNotNull(reconciliationData.get(0));
        assertThrows(UnsupportedOperationException.class,
                () -> reconciliationData.add(new Csv()));
        assertThrows(UnsupportedOperationException.class,
                () -> reconciliationData.remove(0));
    }
}