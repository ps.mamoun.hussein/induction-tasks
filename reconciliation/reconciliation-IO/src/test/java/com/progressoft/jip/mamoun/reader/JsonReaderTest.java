package com.progressoft.jip.mamoun.reader;


import com.progressoft.jip.mamoun.GeneralModel;
import com.progressoft.jip.mamoun.Json;
import com.progressoft.jip.mamoun.reader.JsonReader;
import com.progressoft.jip.mamoun.reader.ReconciliationReader;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class JsonReaderTest {


    @Test
    public void givenNullParam_whenConstruct_thenIllegalArgumentException() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> new JsonReader(null));
        assertEquals(exception.getMessage(), "Reader must not be null");
    }


    @Test
    public void givenValidJsonReader_whenConstruct_thenHappyScenarioWithUnmodifiableList() throws IOException {
        Path path = Paths.get("src/test/resources/online-banking-transactions.json");
        Reader reader = Files.newBufferedReader(path);
        ReconciliationReader reconciliationReader = new JsonReader(reader);
        List<GeneralModel> reconciliationData = reconciliationReader.getReconciliationData();
        assertNotNull(reconciliationData.get(0));
        assertThrows(UnsupportedOperationException.class,
                () -> reconciliationData.add(new Json()));
        assertThrows(UnsupportedOperationException.class,
                () -> reconciliationData.remove(0));
    }


}