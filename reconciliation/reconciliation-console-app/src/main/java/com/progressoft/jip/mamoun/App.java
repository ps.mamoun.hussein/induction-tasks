package com.progressoft.jip.mamoun;

import com.progressoft.jip.mamoun.reader.CsvReader;
import com.progressoft.jip.mamoun.reader.JsonReader;
import com.progressoft.jip.mamoun.reader.ReconciliationReader;
import com.progressoft.jip.mamoun.reader.factory.ReconciliationReaderFactory;
import com.progressoft.jip.mamoun.writer.ReconciliationWriter;
import com.progressoft.jip.mamoun.writer.factory.ReconciliationWriterFactory;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;


// TODO, no tests? How to test an app ??

public class App {
    private static final Scanner sc = new Scanner(System.in);
    private static final HashMap<Path, List<GeneralModel>> dataCaching = new HashMap<>();

    public static void main(String[] args) {
        start();
    }


    // TODO, it is better to keep a method as 15-17 lines or less, extract methods if you can (you can use intellij refactor shortcut ctrl + alt + M)
    //  Example: extract line 47 to line 52 and you can call it as getDataIfCached (DONE)
    private static void start() {
        List<GeneralModel> sourceDataList;
        List<GeneralModel> targetDataList;

        System.out.println("To Clear cache please type 'reset'");
        if (sc.nextLine().equalsIgnoreCase("reset"))
            clearCache();

        System.out.println("Enter source file location: ");
        // TODO, using .next() causes problems as it stops after a space
        //  Example: /My Folder/induction-submitted-tasks/mamoun/induction-tasks/reconciliation/reconciliation-console-app/src/main/resources/online-banking-transactions.json
        //  will be taken as: /My
        //  and the whole system will be broken, use nextLine (also in windows they rarely use folder names like 'My-Folder', it is always 'My Folder') (DONE)
        Path sourcePath = Paths.get(sc.nextLine());

        // TODO, data caching is not a bad idea, but what if the client kept the application running, and the next day downloaded a new file with the same path
        //  and tried to parse it again?, it will get the old file, if caching is involved, it is better to implement a clear cache functionality if required (DONE)
        sourceDataList = getDataIfCached(sourcePath);

        System.out.println("Enter target file location: ");
        Path targetPath = Paths.get(sc.nextLine());

        targetDataList = getDataIfCached(targetPath);

        System.out.println("Please enter the out file extension ");
        ReconciliationWriter writer = getWriter();
        Reconciliation reconciliation = new Reconciliation(sourceDataList, targetDataList);
        writer.writeMatchingFile(reconciliation.getMatchingTransaction());
        writer.writeMisMatchingFile(reconciliation.getMisMatchingTransaction());
        writer.writeMissingFile(reconciliation.getMissingTransaction());


        System.out.println("Files saved successfully in " + writer.getFOLDER_PATH());


        dataCaching.putIfAbsent(sourcePath, sourceDataList);
        dataCaching.putIfAbsent(targetPath, targetDataList);

        System.out.println("Want to check another file (y/N)? ");
        if (sc.nextLine().equalsIgnoreCase("y")) start();
        else System.out.println("Finish App");
    }

    private static void clearCache() {
        dataCaching.clear();
    }

    private static List<GeneralModel> getDataIfCached(Path sourcePath) {
        List<GeneralModel> sourceDataList;
        if (dataCaching.containsKey(sourcePath)) {
            sourceDataList = dataCaching.get(sourcePath);
        } else {
            ReconciliationReader sourceReader = getReader(sourcePath);
            sourceDataList = sourceReader.getReconciliationData();
        }
        return sourceDataList;
    }

    private static ReconciliationWriter getWriter() {
        try {
            return ReconciliationWriterFactory.getWriter(sc.nextLine());
        } catch (IllegalArgumentException e) {
            if (e.getMessage().equalsIgnoreCase("Unsupported extension")) {
                System.out.println("The format you entered is not supported yet please choose from");
                System.out.println(ReconciliationWriterFactory.supportedExtensions);
                return getWriter();
            }
            throw e;
        }
    }

    private static ReconciliationReader getReader(Path path) {
        try {
            return ReconciliationReaderFactory.getReader(path);
        } catch (IllegalArgumentException e) {
            // TODO Note, even tho my application failed to detect file extension, it asked me to enter a new file extension
            //  but instead of waiting me to enter, it just went out of switch statement and threw an illegal argument exception (sc.next() is the cause)
            //  NOTE: sc.next() did not work because the buffer had some buffer inside of it, example: if a user enters a path like /My Folder/
            //  it will take My and Folder into 2 separate inputs (DONE)
            if (e.getMessage().equals("Unknown file format")) {
                System.out.println("Failed to detect file extension please enter file extension for " + path);
                switch (sc.nextLine().toLowerCase()) {
                    case "csv":
                        return new CsvReader(getBufferReader(path));
                    case "json":
                        return new JsonReader(getBufferReader(path));
                }
            }
            throw new IllegalArgumentException(e);
        }
    }


    // TODO Optional, inconsistent way of checking for the file, (This todo is given as a third party library - not as a console based app)
    //  Example: inside ReconciliationReaderFactory Class, if the file is not found, an exception is thrown and the application ends
    //  (not a bad way, it is actually great for developers that uses your application as a third party library)
    //  However, in this method, if the File is not found, you are asking the user to enter another file path
    //  this will confuse the developers using this as a third party as they wont know when the application will ask for another path or throw an exception (unless they trace the application) (DONE)
    private static Reader getBufferReader(Path path) {
        try {
            return Files.newBufferedReader(path);
        } catch (IOException e) {
            System.out.println("File not found");
            System.out.println("Please enter another file path for " + path);
            return getBufferReader(Paths.get(sc.nextLine()));
        }
    }
}
