package com.progressoft.jip.mamoun.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomDateParser {

    private static final String DESIRED_DATE_FORMAT = "yyyy-MM-dd";


    public static String parse(Date date) {
        if (date == null) throw new IllegalArgumentException("Can't parse null date");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DESIRED_DATE_FORMAT);
        return simpleDateFormat.format(date);
    }

    public static String parse(String date, String inputFormat) {
        checkData(date);
        checkData(inputFormat);
        return parse(getDate(date, inputFormat));
    }


    public static Date getDate(String date, String inputFormat) {
        checkData(date);
        checkData(inputFormat);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(inputFormat);
        try {
            return simpleDateFormat.parse(date);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Could not parse date into format of: " + inputFormat);
        }
    }

    private static void checkData(String data) {
        if (data == null || data.isEmpty()) throw new IllegalArgumentException("Date or format can't be null or empty");
    }

}
