package com.progressoft.jip.mamoun;


import java.util.*;


// TODO, there are no validations for missing fields,
//  Example: JSON file with a transcation that has the amount field missing (DONE)
public class Reconciliation {
    private final List<GeneralModel> source;
    private final List<GeneralModel> target;
    private final List<GeneralModel> matchingTransaction = new ArrayList<>();

    private final List<GeneralMissingModel> misMatchingTransaction = new ArrayList<>();

    private final List<GeneralMissingModel> missingTransaction = new ArrayList<>();

    private final Set<String> idSet = new HashSet<>();

    public Reconciliation(List<GeneralModel> source, List<GeneralModel> target) {
        checkFile(source, target);
        this.source = source;
        this.target = target;
        generateData();
    }

    private void checkFile(List<GeneralModel> source, List<GeneralModel> target) {
        if (source == null) throw new IllegalArgumentException("The source file must not be null");
        if (target == null) throw new IllegalArgumentException("The target file must not be null");
    }

    private void generateData() {
        List<GeneralModel> tempSourceList = new ArrayList<>(source);
        List<GeneralModel> tempTargetList = new ArrayList<>(target);
        for (GeneralModel sourceData : source) {
            for (GeneralModel targetData : target) {
                if (sourceData.getTransactionId().equals(targetData.getTransactionId())) {
                    checkTransactionId(sourceData);
                    tempSourceList.remove(sourceData);
                    tempTargetList.remove(targetData);
                    if (isMatching(sourceData, targetData)) {
                        this.matchingTransaction.add(sourceData);
                    } else {
                        this.misMatchingTransaction.add(new MissingModel(sourceData, "SOURCE"));
                        this.misMatchingTransaction.add(new MissingModel(targetData, "TARGET"));
                    }
                    idSet.add(sourceData.getTransactionId());
                }
            }
        }

        addMissingData(tempSourceList, "SOURCE");
        addMissingData(tempTargetList, "TARGET");
    }

    private void checkTransactionId(GeneralModel sourceData) {
        if (idSet.contains(sourceData.getTransactionId())) {
            throw new IllegalStateException("Repeated transaction id for more than one transaction");
        }
    }

    private void addMissingData(List<GeneralModel> source, String fileSource) {
        source.forEach(data -> this.missingTransaction.add(new MissingModel(data, fileSource)));
    }

    // TODO separate like this or a lot of 'if' -- >> This TODO is written by Mamoun (DONE)
    private boolean isMatching(GeneralModel sourceData, GeneralModel targetData) {
        try {
            boolean isSameAmount = sourceData.getAmount().compareTo(targetData.getAmount()) == 0;
            boolean isSameDate = sourceData.getValueDate().equals(targetData.getValueDate());
            boolean isSameCurrencyCode = sourceData.getCurrencyCode().equals(targetData.getCurrencyCode());
            return isSameAmount && isSameDate && isSameCurrencyCode;
        } catch (Exception e) {
            throw new IllegalStateException("Missing data");
        }

    }


    public List<GeneralModel> getMatchingTransaction() {

        return Collections.unmodifiableList(this.matchingTransaction);
    }

    public List<GeneralMissingModel> getMisMatchingTransaction() {
        return Collections.unmodifiableList(this.misMatchingTransaction);
    }

    public List<GeneralMissingModel> getMissingTransaction() {
        return Collections.unmodifiableList(this.missingTransaction);
    }


    public List<GeneralModel> getSource() {
        return Collections.unmodifiableList(source);
    }

    public List<GeneralModel> getTarget() {
        return Collections.unmodifiableList(target);
    }
}
