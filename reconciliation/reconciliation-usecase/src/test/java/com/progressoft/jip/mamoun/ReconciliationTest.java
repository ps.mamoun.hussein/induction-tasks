package com.progressoft.jip.mamoun;


import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.jupiter.api.Assertions.*;

public class ReconciliationTest {


    public static final int DATA_SIZE = 50;

    @Test
    public void givenNullParam_WhenConstruct_ThenIllegalArgumentException() {
        List<GeneralModel> sourceFile = new ArrayList<>();
        List<GeneralModel> targetFile = new ArrayList<>();
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> new Reconciliation(null, targetFile));
        assertEquals(exception.getMessage(), "The source file must not be null");

        exception = assertThrows(IllegalArgumentException.class, () -> new Reconciliation(sourceFile, null));
        assertEquals(exception.getMessage(), "The target file must not be null");
    }

    @Test
    public void givenTwoListOfData_whenConstruct_thenHappyScenario() throws ParseException {
        List<GeneralModel> sourceData = generateRandomData();
        List<GeneralModel> targetData = generateRandomData();
        Reconciliation reconciliation = new Reconciliation(sourceData, targetData);
        assertTrue(reconciliation.getMatchingTransaction().size() <= DATA_SIZE);
        assertTrue(reconciliation.getMissingTransaction().size() <= DATA_SIZE * 2);
        assertTrue(reconciliation.getMisMatchingTransaction().size() <= DATA_SIZE * 2);
    }

    private List<GeneralModel> generateRandomData() throws ParseException {
        List<GeneralModel> data = new ArrayList<>();
        for (int i = 0; i < DATA_SIZE; i++) {
            String transId = randomStringMix();
            int randomAmount = randomNumber();
            String randomCurrencyCode = randomStringMix();
            Date randomDate = generateRandomDate();
            GeneralModel model = getModel(transId, randomAmount, randomCurrencyCode, randomDate);
            data.add(model);
        }
        return Collections.unmodifiableList(data);
    }

    private GeneralModel getModel(String transId, int randomAmount, String randomCurrencyCode, Date randomDate) {
        return new GeneralModel() {
            @Override
            public String getTransactionId() {
                return transId;
            }

            @Override
            public BigDecimal getAmount() {
                return new BigDecimal(randomAmount);
            }

            @Override
            public String getCurrencyCode() {
                return randomCurrencyCode;
            }

            @Override
            public Date getValueDate() {
                return randomDate;
            }
        };
    }


    private int randomNumber() {
        return new Random().nextInt(26);
    }

    private String randomStringMix() {
        StringBuilder stringBuilder = new StringBuilder();
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for (int i = 0; i < 3; i++) {
            stringBuilder.append(alphabet.charAt(randomNumber() % alphabet.length()));
        }
        return stringBuilder.toString();
    }


    private Date generateRandomDate() throws ParseException {
        String start = "08-05-2022";
        String end = "24-05-2022";
        String format = "dd-MM-yyyy";

        Date startMillis = new SimpleDateFormat(format).parse(start);
        Date endMillis = new SimpleDateFormat(format).parse(end);
        long randomMillisSinceEpoch = ThreadLocalRandom
                .current()
                .nextLong(startMillis.getTime(), endMillis.getTime());

        return new Date(randomMillisSinceEpoch);
    }

}
