package com.progressoft.jip.mamoun.date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class DateParserTest {


    @Test
    public void givenNullParams_whenCallingMethods_thenIllegalArgumentException() {
        final String VALID_DATE = "23-05-2022";
        final String VALID_FORMAT = "yyyy-MM-dd";
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> CustomDateParser.parse(null));
        assertEquals(exception.getMessage(), "Can't parse null date");
        exception = Assertions.assertThrows(IllegalArgumentException.class, () -> CustomDateParser.parse(null, VALID_FORMAT));
        assertEquals(exception.getMessage(), "Date or format can't be null or empty");
        exception = Assertions.assertThrows(IllegalArgumentException.class, () -> CustomDateParser.parse(VALID_DATE, null));
        assertEquals(exception.getMessage(), "Date or format can't be null or empty");
        exception = Assertions.assertThrows(IllegalArgumentException.class, () -> CustomDateParser.getDate(null, VALID_FORMAT));
        assertEquals(exception.getMessage(), "Date or format can't be null or empty");
        exception = Assertions.assertThrows(IllegalArgumentException.class, () -> CustomDateParser.getDate(VALID_DATE, null));
        assertEquals(exception.getMessage(), "Date or format can't be null or empty");
        exception = Assertions.assertThrows(IllegalArgumentException.class, () -> CustomDateParser.getDate(VALID_DATE, ""));
        assertEquals(exception.getMessage(), "Date or format can't be null or empty");
        exception = Assertions.assertThrows(IllegalArgumentException.class, () -> CustomDateParser.getDate("", VALID_FORMAT));
        assertEquals(exception.getMessage(), "Date or format can't be null or empty");
        exception = Assertions.assertThrows(IllegalArgumentException.class, () -> CustomDateParser.parse(VALID_DATE, ""));
        assertEquals(exception.getMessage(), "Date or format can't be null or empty");
        exception = Assertions.assertThrows(IllegalArgumentException.class, () -> CustomDateParser.parse(null, null));
        assertEquals(exception.getMessage(), "Date or format can't be null or empty");
    }

    @Test
    public void givenInvalidDateFormat_whenCallingMethod_thenIllegalArgumentException() {
        final String VALID_DATE = "23-05-2022";
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> CustomDateParser.parse(VALID_DATE, "dd/MM/yyyy"));
        assertEquals(exception.getMessage(), "Could not parse date into format of: dd/MM/yyyy");
        exception = Assertions.assertThrows(IllegalArgumentException.class, () -> CustomDateParser.getDate(VALID_DATE, "dd/MM/yyyy"));
        assertEquals(exception.getMessage(), "Could not parse date into format of: dd/MM/yyyy");
    }

    @Test
    public void givenValidDateAndFormat_whenCallingMethod_thenHappyScenario() {
        final String VALID_DATE = "23/05/2022";
        final String VALID_FORMAT = "dd/MM/yyyy";

        Date date = CustomDateParser.getDate(VALID_DATE, VALID_FORMAT);
        assertNotNull(date);
        String dateString = CustomDateParser.parse(VALID_DATE, VALID_FORMAT);
        assertNotNull(dateString);
        assertEquals("2022-05-23", dateString);
        assertEquals(dateString, CustomDateParser.parse(date));

    }

}