package com.progressoft.jip.mamoun.dao;

import com.progressoft.jip.mamoun.entity.ActivityEntity;

import java.util.List;

public interface ActivityDao {

    List<ActivityEntity> getActivity();

    ActivityEntity findActivity(long id);

    ActivityEntity saveActivity(ActivityEntity activity);

    ActivityEntity updateActivity(ActivityEntity activity);

    ActivityEntity deleteActivity(long id);
}
