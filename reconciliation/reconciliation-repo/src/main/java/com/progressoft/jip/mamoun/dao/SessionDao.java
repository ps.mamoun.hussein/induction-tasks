package com.progressoft.jip.mamoun.dao;

import com.progressoft.jip.mamoun.entity.UserSessionEntity;

import java.util.List;

public interface SessionDao {

    List<UserSessionEntity> getSessions();

    UserSessionEntity findSession(long id);

    UserSessionEntity saveSession(UserSessionEntity session);

    UserSessionEntity updateSession(UserSessionEntity session);

    UserSessionEntity deleteSession(long id);
}
