package com.progressoft.jip.mamoun.entity;

import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Table(name = "USER_SESSION")
@Entity
public class UserSessionEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long id;


    @CreationTimestamp
    @Column(name = "time")
    private Date time;

    @Column(name = "user_id", nullable = false)
    private long userID;


    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }


    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "session_id")
    private Set<ActivityEntity> activityEntities = new HashSet<>();

    public List<ActivityEntity> getActivityEntities() {
        return new ArrayList<>(activityEntities);
    }


    public Long getId() {
        return id;
    }


    public Date getTime() {
        return time;
    }

    public void setTime(Date date) {
        this.time = date;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        UserSessionEntity that = (UserSessionEntity) o;
        return this.id == that.getId();
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }


    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "id = " + id + ", " +
                "time = " + time + ", " +
                "userID = " + userID + ")";
    }
}
