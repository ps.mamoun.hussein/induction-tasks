package com.progressoft.jip.mamoun.entity;


import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Table(name = "RECONCILIATION_ACTIVITY")
@Entity
public class ActivityEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long id;


    @CreationTimestamp
    @Column(name = "time", nullable = false)
    private Date time;


    @Column(name = "sourceName")
    private String sourceName;


    @Column(name = "targetName")
    private String targetName;

    @Column(name = "matched")
    private int matched;

    @Column(name = "mismatched")
    private int mismatched;

    @Column(name = "missing")
    private int missing;

    @Column(name = "session_id", nullable = false)
    private long sessionID;

    public long getSessionID() {
        return sessionID;
    }

    public void setSessionID(long sessionID) {
        this.sessionID = sessionID;
    }


    public long getId() {
        return id;
    }

    public Date getTime() {
        return time;
    }


    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getTargetName() {
        return targetName;
    }

    public void setTargetName(String targetName) {
        this.targetName = targetName;
    }

    public int getMatched() {
        return matched;
    }

    public void setMatched(int matched) {
        this.matched = matched;
    }

    public int getMismatched() {
        return mismatched;
    }

    public void setMismatched(int mismatched) {
        this.mismatched = mismatched;
    }

    public int getMissing() {
        return missing;
    }

    public void setMissing(int missing) {
        this.missing = missing;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ActivityEntity that = (ActivityEntity) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }


    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "id = " + id + ", " +
                "time = " + time + ", " +
                "sourceName = " + sourceName + ", " +
                "targetName = " + targetName + ", " +
                "matched = " + matched + ", " +
                "mismatched = " + mismatched + ", " +
                "missing = " + missing + ", " +
                "sessionID = " + sessionID + ")";
    }
}
