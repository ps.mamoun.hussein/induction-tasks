package com.progressoft.jip.mamoun.dao.imp;

import com.progressoft.jip.mamoun.dao.UserDao;
import com.progressoft.jip.mamoun.entity.UserEntity;
import com.progressoft.jip.mamoun.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;
import java.util.Collections;
import java.util.List;

import static com.progressoft.jip.mamoun.util.PasswordUtil.hashPassword;
import static com.progressoft.jip.mamoun.util.PasswordUtil.isEncrypted;

public class UserDaoImp implements UserDao {

    private final SessionFactory sessionFactory;

    public UserDaoImp() {
        sessionFactory = HibernateUtil.getSessionFactory();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<UserEntity> getUsers() {
        try (Session session = sessionFactory.openSession()) {
            String hql = "from UserEntity";
            Query<UserEntity> query = session.createQuery(hql);
            return Collections.unmodifiableList(query.list());
        }
    }

    @Override
    public UserEntity findUser(long id) {
        try (Session session = sessionFactory.openSession()) {
            return session.find(UserEntity.class, id);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public UserEntity findUser(String username) {
        try (Session session = sessionFactory.openSession()) {
            String hql = "from UserEntity where username = :username";
            Query<UserEntity> query = session.createQuery(hql);
            query.setParameter("username", username.toLowerCase());
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public UserEntity saveUser(UserEntity user) throws IllegalArgumentException {
        checkUser(user);
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            cryptPassword(user);
            session.save(user);
            transaction.commit();
            return user;
        }
    }

    @Override
    public UserEntity updateUser(UserEntity user) {
        checkUser(user);
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            cryptPassword(user);
            session.update(user);
            transaction.commit();
            return user;
        } catch (OptimisticLockException e) {
            throw new IllegalArgumentException(user + "Could not be found in the database to update it!, are you sure you are calling the right method");
        }
    }


    @Override
    public UserEntity deleteUser(long id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            UserEntity user = findUser(id);
            if (user != null)
                session.delete(user);
            transaction.commit();
            return user;
        }
    }

    private void checkUser(UserEntity user) {
        if (user == null || user.getUsername() == null || user.getPassword() == null) {
            throw new IllegalArgumentException("User, username and password must not be null");
        }
    }

    private void cryptPassword(UserEntity user) {
        if (!isEncrypted(user.getPassword()))
            user.setPassword(hashPassword(user.getPassword()));
    }
}
