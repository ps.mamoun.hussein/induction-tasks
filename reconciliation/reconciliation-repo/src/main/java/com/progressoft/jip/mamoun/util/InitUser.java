package com.progressoft.jip.mamoun.util;

import com.progressoft.jip.mamoun.dao.UserDao;
import com.progressoft.jip.mamoun.dao.imp.UserDaoImp;
import com.progressoft.jip.mamoun.entity.UserEntity;

public abstract class InitUser {


    public static void initUsers() {
        UserDao userDao = new UserDaoImp();
        if (userDao.findUser("talal") == null)
            createUser("Talal", userDao);

        if (userDao.findUser("George") == null)
            createUser("George", userDao);

        if (userDao.findUser("osAma") == null)
            createUser("Osama", userDao);
    }

    private static void createUser(String username, UserDao userDao) {
        UserEntity talal = new UserEntity();
        talal.setUsername(username);
        talal.setPassword("P@ssw0rd");
        userDao.saveUser(talal);
    }
}
