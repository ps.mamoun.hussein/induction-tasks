package com.progressoft.jip.mamoun.dao;


import com.progressoft.jip.mamoun.entity.UserEntity;

import java.util.List;

public interface UserDao {

    List<UserEntity> getUsers();

    UserEntity findUser(long id);

    UserEntity findUser(String userName);


    UserEntity saveUser(UserEntity user) throws IllegalArgumentException;

    UserEntity updateUser(UserEntity user) throws IllegalArgumentException;

    UserEntity deleteUser(long id) throws IllegalArgumentException;

}
