package com.progressoft.jip.mamoun.dao.imp;

import com.progressoft.jip.mamoun.Currency;
import com.progressoft.jip.mamoun.dao.CurrencyDao;
import com.progressoft.jip.mamoun.data.CurrencyDataBase;

import java.util.Collections;
import java.util.List;

public class CurrencyDaoImp implements CurrencyDao {

    private final CurrencyDataBase currencyDataBase;

    public CurrencyDaoImp() {
        currencyDataBase = CurrencyDataBase.getINSTANCE();
    }


    public int getDecimal(String currencyCode) {
        return getCurrencyByCode(currencyCode).getDecimalPoint();
    }

    @Override
    public List<Currency> getAllCurrencies() {
        return Collections.unmodifiableList(currencyDataBase.getAllCurrencies());
    }

    @Override
    public Currency getCurrencyByCode(String currencyCode) {
        return currencyDataBase.getCurrencyByCode(currencyCode);
    }

}
