package com.progressoft.jip.mamoun.util;

import com.progressoft.jip.mamoun.entity.ActivityEntity;
import com.progressoft.jip.mamoun.entity.UserEntity;
import com.progressoft.jip.mamoun.entity.UserSessionEntity;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public abstract class HibernateUtil {

    private volatile static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            synchronized (HibernateUtil.class) {
                if (sessionFactory == null) {
                    initSession();
                }
            }
        }
        return sessionFactory;
    }

    private static void initSession() {
        Configuration configuration = new Configuration();
        sessionFactory = configuration
                .addAnnotatedClass(UserEntity.class)
                .addAnnotatedClass(UserSessionEntity.class)
                .addAnnotatedClass(ActivityEntity.class)
                .buildSessionFactory();
    }


}
