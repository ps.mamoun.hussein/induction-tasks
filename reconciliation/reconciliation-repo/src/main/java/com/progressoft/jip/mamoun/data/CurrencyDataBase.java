package com.progressoft.jip.mamoun.data;




import com.opencsv.bean.CsvToBeanBuilder;
import com.progressoft.jip.mamoun.Currency;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

// this is a dummy class represent database
public class CurrencyDataBase {

    private static volatile CurrencyDataBase INSTANCE;
    private final List<Currency> currencies;


    private CurrencyDataBase() {
        try {
            String filePath = "currencyCode.csv";
            URL resource = CurrencyDataBase.class.getClassLoader().getResource(filePath);
            if (resource == null) throw new IOException();
            currencies = new CsvToBeanBuilder<Currency>(Files.newBufferedReader(Paths.get(resource.toURI()))).withSkipLines(1).withType(Currency.class).build().parse();
            System.out.println(currencies);
        } catch (IOException | URISyntaxException e) {
            throw new IllegalArgumentException("Please check the currencyCode.csv is available in the resource folder");
        }
    }

    // singleton pattern
    public static CurrencyDataBase getINSTANCE() {
        if (INSTANCE == null) {
            synchronized (CurrencyDataBase.class) {
                if (INSTANCE == null) INSTANCE = new CurrencyDataBase();
            }
        }
        return INSTANCE;
    }

    public List<Currency> getAllCurrencies() {
        return Collections.unmodifiableList(currencies);
    }


    public Currency getCurrencyByCode(String currencyCode) {
        if (currencyCode == null || currencyCode.isEmpty()) {
            throw new IllegalArgumentException("Currency Code must not be null or empty");
        }
        for (Currency currency : currencies) {
            if (currency.getCurrencyCode().equals(currencyCode))
                return currency;
        }
        // TODO Optional, an exception is better than returning null
        //  However, some developers prefers it as null for a specific workflow (DONE)
        throw new IllegalArgumentException("Could not find currency you are looking for");
    }
}
