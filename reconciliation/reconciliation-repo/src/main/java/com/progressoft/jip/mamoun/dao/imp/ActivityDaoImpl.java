package com.progressoft.jip.mamoun.dao.imp;

import com.progressoft.jip.mamoun.dao.ActivityDao;
import com.progressoft.jip.mamoun.entity.ActivityEntity;
import com.progressoft.jip.mamoun.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.OptimisticLockException;
import java.util.List;

public class ActivityDaoImpl implements ActivityDao {
    private final SessionFactory sessionFactory;

    public ActivityDaoImpl() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ActivityEntity> getActivity() {
        try (Session session = sessionFactory.openSession()) {
            String hql = "from ActivityEntity";
            Query<ActivityEntity> query = session.createQuery(hql);
            return query.list();
        }
    }

    @Override
    public ActivityEntity findActivity(long id) {
        try (Session session = sessionFactory.openSession()) {
            return session.find(ActivityEntity.class, id);
        }
    }

    @Override
    public ActivityEntity saveActivity(ActivityEntity activity) {
        checkActivity(activity);
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.persist(activity);
            transaction.commit();
            return activity;
        }
    }

    @Override
    public ActivityEntity updateActivity(ActivityEntity activity) {
        checkActivity(activity);
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(activity);
            transaction.commit();
            return activity;
        } catch (OptimisticLockException e) {
            throw new IllegalArgumentException(activity + "Could not be found in the database to update it!, are you sure you are calling the right method");
        }
    }

    @Override
    public ActivityEntity deleteActivity(long id) {
        try (Session session = sessionFactory.openSession()) {
            ActivityEntity activity = findActivity(id);
            Transaction transaction = session.beginTransaction();
            if (activity != null)
                session.delete(activity);
            transaction.commit();
            return activity;
        }
    }

    private void checkActivity(ActivityEntity activity) {
        if (activity == null )
            throw new IllegalArgumentException("Activity or activity session must not be null");
    }
}
