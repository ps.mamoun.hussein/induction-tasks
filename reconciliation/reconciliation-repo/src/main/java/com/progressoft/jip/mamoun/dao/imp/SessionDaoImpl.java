package com.progressoft.jip.mamoun.dao.imp;

import com.progressoft.jip.mamoun.dao.SessionDao;
import com.progressoft.jip.mamoun.entity.UserSessionEntity;
import com.progressoft.jip.mamoun.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.OptimisticLockException;
import java.util.List;

public class SessionDaoImpl implements SessionDao {

    private final SessionFactory sessionFactory;


    public SessionDaoImpl() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<UserSessionEntity> getSessions() {
        try (Session session = sessionFactory.openSession()) {
            String hql = "from UserSessionEntity";
            Query<UserSessionEntity> query = session.createQuery(hql);
            return query.list();
        }
    }

    @Override
    public UserSessionEntity findSession(long id) {
        try (Session session = sessionFactory.openSession()) {
            return session.find(UserSessionEntity.class, id);
        }
    }

    @Override
    public UserSessionEntity saveSession(UserSessionEntity userSessionEntity) {
        checkSession(userSessionEntity);
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(userSessionEntity);
            transaction.commit();
            return userSessionEntity;
        }
    }


    @Override
    public UserSessionEntity updateSession(UserSessionEntity userSessionEntity) {
        checkSession(userSessionEntity);
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(userSessionEntity);
            transaction.commit();
            return userSessionEntity;
        } catch (OptimisticLockException e) {
            throw new IllegalArgumentException(userSessionEntity + "Could not be found in the database to update it!, are you sure you are calling the right method");
        }
    }

    @Override
    public UserSessionEntity deleteSession(long id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            UserSessionEntity userSessionEntity = findSession(id);
            if (userSessionEntity != null)
                session.delete(userSessionEntity);
            transaction.commit();
            return userSessionEntity;
        }
    }


    private void checkSession(UserSessionEntity userSessionEntity) {
        if (userSessionEntity == null)
            throw new IllegalArgumentException("'User Session' and 'User' must not be null");
    }

}
