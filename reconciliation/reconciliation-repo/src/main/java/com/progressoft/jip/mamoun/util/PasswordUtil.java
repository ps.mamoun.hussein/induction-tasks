package com.progressoft.jip.mamoun.util;

import org.mindrot.jbcrypt.BCrypt;

import java.util.regex.Pattern;

public abstract class PasswordUtil {

    private static final int WORKLOAD = 12;
    public static final String SALT = BCrypt.gensalt(WORKLOAD);
    public static final String CRYPT_REGEX = "\\A\\$2a?\\$\\d\\d\\$[./0-9A-Za-z]{53}"; // from Bcrypt documentation
    public static final Pattern CRYPT_PATTERN = Pattern.compile(CRYPT_REGEX);

    public static final String PASSWORD_REGEX = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$";

    public static final Pattern PASSWORD_PATTERN = Pattern.compile(PASSWORD_REGEX);

    public static String hashPassword(String password_plaintext) {
        checkPasswordNull(password_plaintext);
        if (!isPassWordStrong(password_plaintext))
            throw new IllegalArgumentException("Please make sure that your password is minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character");
        return BCrypt.hashpw(password_plaintext, SALT);
    }


    public static boolean isEncrypted(String password_plaintext) {
        checkPasswordNull(password_plaintext);
        return CRYPT_PATTERN.matcher(password_plaintext).matches();
    }


    public static boolean isPassWordStrong(String password_plaintext) {
        checkPasswordNull(password_plaintext);
        return PASSWORD_PATTERN.matcher(password_plaintext).matches();
    }

    public static boolean isPasswordMatching(String plainPassword, String hashed) {
        boolean checkpw = BCrypt.checkpw(plainPassword, hashed);
        return checkpw;
    }

    private static void checkPasswordNull(String password_plaintext) {
        if (password_plaintext == null)
            throw new IllegalArgumentException("Password can't be null");
    }

}
