package com.progressoft.jip.mamoun.dao;

import com.progressoft.jip.mamoun.Currency;

import java.util.List;

public interface CurrencyDao {

    int getDecimal(String currencyCode);

    List<Currency> getAllCurrencies();

    Currency getCurrencyByCode(String currencyCode);
}
