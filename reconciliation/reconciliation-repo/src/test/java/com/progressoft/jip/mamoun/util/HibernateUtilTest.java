package com.progressoft.jip.mamoun.util;

import org.hibernate.SessionFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HibernateUtilTest {


    private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();


    @Test
    @Disabled
    public void givenNoPropertiesFile_whenCallingGetSession_thenIllegalArgumentException() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                HibernateUtil::getSessionFactory);
        assertEquals(exception.getMessage(), "Please make sure to provide 'hibernate.properties' file");

    }

    @Test
    public void givenHibernateUtil_whenCallingGetSession_thenReturningSameObject() {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        assertEquals(sessionFactory, this.sessionFactory);
        assertEquals(sessionFactory.hashCode(), this.sessionFactory.hashCode());
    }
}