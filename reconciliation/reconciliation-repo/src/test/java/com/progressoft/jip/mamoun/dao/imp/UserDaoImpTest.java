package com.progressoft.jip.mamoun.dao.imp;

import com.progressoft.jip.mamoun.dao.UserDao;
import com.progressoft.jip.mamoun.entity.UserEntity;
import com.progressoft.jip.mamoun.util.HibernateUtil;
import com.progressoft.jip.mamoun.util.PasswordUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


class UserDaoImpTest {


    private static final Session session = HibernateUtil.getSessionFactory().openSession();

    private static final String PASSWORD = "P@ssw0rd";

    private static final String PASSWORD2 = "Progress0ft@";
    private final UserDao userDao;

    private long validID = 0;

    public UserDaoImpTest() {
        this.userDao = new UserDaoImp();
    }


    @BeforeEach
    public void addData() {
        clearDB();
        insertData();
    }


    @Test
    public void givenNullUsernameOrPassword_whenSavingUser_thenGivenException() {
        UserEntity user = new UserEntity();
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> userDao.saveUser(user));
        assertEquals(exception.getMessage(), "User, username and password must not be null");
        user.setUsername("Hello");
        exception = assertThrows(IllegalArgumentException.class,
                () -> userDao.saveUser(user));
        assertEquals(exception.getMessage(), "User, username and password must not be null");
        user.setPassword("P@ssw0rd");
        user.setUsername(null);
        exception = assertThrows(IllegalArgumentException.class,
                () -> userDao.saveUser(user));
        assertEquals(exception.getMessage(), "User, username and password must not be null");
        exception = assertThrows(IllegalArgumentException.class,
                () -> userDao.saveUser(null));
        assertEquals(exception.getMessage(), "User, username and password must not be null");
    }

    @Test
    public void givenNullUsername_whenUpdatingUser_thenGivenException() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> userDao.updateUser(null));
        assertEquals(exception.getMessage(), "User, username and password must not be null");
        UserEntity user = userDao.findUser(validID);
        List<UserEntity> users = userDao.getUsers();
        System.out.println(users);
        user.setUsername(null);
        user.setPassword("P@ssw0rd");
        exception = assertThrows(IllegalArgumentException.class,
                () -> userDao.updateUser(user));
        assertEquals(exception.getMessage(), "User, username and password must not be null");
    }

    @Test
    public void givenNewValidNotSavedUser_whenCallingUpdate_thenIllegalArgumentException() {
        UserEntity user = new UserEntity();
        user.setUsername("Osama");
        user.setPassword(PASSWORD);
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> userDao.updateUser(user));
        assertEquals(exception.getMessage(), user + "Could not be found in the database to update it!, are you sure you are calling the right method");
    }

    @Test
    public void givenWrongId_whenDeletingOrFindingUser_thenReturningNull() {
        assertNull(userDao.findUser(100));
        assertNull(userDao.deleteUser(150));
    }


    @Test
    public void whenCallingGetUsers_thenReturningListContaminatingAllUsers() {
        assertEquals(userDao.getUsers().size(), 2);
    }

    @Test
    public void givenValidUserToUpdate_whenCallingUpdateUser_thenReturningTheUpdatedUser() {
        UserEntity user = userDao.findUser(validID);
        user.setUsername("Ahmed");
        user.setPassword(PASSWORD2);
        UserEntity updateUser = userDao.updateUser(user);
        assertEquals(updateUser, user);
        assertEquals(user.getPassword(), updateUser.getPassword());
        assertNotNull(updateUser);
        assertEquals(userDao.findUser(validID).getUsername(), "ahmed");
        assertNotEquals(PasswordUtil.hashPassword(PASSWORD), user.getPassword());
        assertEquals(PasswordUtil.hashPassword(PASSWORD2), user.getPassword());
        assertNotEquals(PASSWORD2, user.getPassword());
    }

    @Test
    public void givenValidUserToInsert_whenCallingSaveUser_thenReturningSavedUser() {
        UserEntity user = new UserEntity();
        user.setUsername("Talal");
        user.setPassword(PASSWORD2);
        UserEntity savedUser = userDao.saveUser(user);
        assertEquals(userDao.getUsers().size(), 3);
        assertEquals(user.getUsername(), savedUser.getUsername());
        assertEquals(user, savedUser);
    }

    @Test
    public void givenValidUserId_whenCallingDelete_thenDeleteUserAndReturningIt() {
        UserEntity user = userDao.findUser(validID);
        UserEntity deletedUser = userDao.deleteUser(validID);
        UserEntity userAfterDelete = userDao.findUser(validID);
        assertEquals(user, deletedUser);
        assertNull(userAfterDelete);
        assertEquals(userDao.getUsers().size(), 1);
    }

    @Test
    public void givenValidUserName_whenCallingFind_thenReturningUser() {
        UserEntity user = userDao.findUser("Mamoun");
        UserEntity deletedUser = userDao.deleteUser(validID);
        UserEntity userAfterDelete = userDao.findUser(validID);
        assertEquals(user, deletedUser);
        assertNull(userAfterDelete);
        assertEquals(userDao.getUsers().size(), 1);
    }

    @SuppressWarnings("unchecked")
    private static void clearDB() {
        Transaction transaction;
        if (session.getTransaction().isActive())
            transaction = session.getTransaction();
        else
            transaction = session.beginTransaction();
        session.createQuery("delete from ActivityEntity ").executeUpdate();
        session.createQuery("delete from UserSessionEntity ").executeUpdate();
        session.createQuery("delete from UserEntity").executeUpdate();
        transaction.commit();
        List<UserEntity> userEntity = session.createQuery("from UserEntity").list();
        assertEquals(userEntity.size(), 0);
    }

    private void insertData() {
        UserEntity user = new UserEntity();
        user.setUsername("Mamoun");
        user.setPassword(PASSWORD);
        Transaction transaction = session.beginTransaction();
        session.save(user);
        validID = user.getId();
        UserEntity user1 = new UserEntity();
        user1.setUsername("Msh Mamoun");
        user1.setPassword(PASSWORD);
        session.save(user1);
        transaction.commit();
        assertEquals(session.createQuery("from UserEntity").list().size(), 2);
    }
}