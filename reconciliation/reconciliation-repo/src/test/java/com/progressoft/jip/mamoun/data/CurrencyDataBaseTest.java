package com.progressoft.jip.mamoun.data;

import com.progressoft.jip.mamoun.Currency;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CurrencyDataBaseTest {

    CurrencyDataBase currencyDataBase = CurrencyDataBase.getINSTANCE();

    @Test
    public void givenNullCurrencyCode_whenCallingGetCurrencyByCode_thenIllegalArgumentException() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> currencyDataBase.getCurrencyByCode(null));
        assertEquals(exception.getMessage(), "Currency Code must not be null or empty");
        exception = assertThrows(IllegalArgumentException.class,
                () -> currencyDataBase.getCurrencyByCode(""));
        assertEquals(exception.getMessage(), "Currency Code must not be null or empty");
    }

    @Test
    public void whenCreatingNewInstance_thenReturnNewOneIfNotCreatedBeforeAndTheSameIfFoundBefore_singletonPattern() {
        assertEquals(currencyDataBase, CurrencyDataBase.getINSTANCE());
        assertEquals(currencyDataBase.hashCode(), CurrencyDataBase.getINSTANCE().hashCode());
    }

    @Test
    public void whenTryingToEditGetAllList_thenUnsupportedOperationException() {
        assertThrows(UnsupportedOperationException.class,
                () -> currencyDataBase.getAllCurrencies().add(new Currency()));
        assertThrows(UnsupportedOperationException.class,
                () -> currencyDataBase.getAllCurrencies().remove(0));
    }


    @Test
    public void givenCorrectCurrencyCode_whenCallingGetCurrencyByCode_thenGetCurrency() {
        Currency currency = currencyDataBase.getCurrencyByCode("JOD");
        assertEquals(currency.getCurrencyCode(), "JOD");
        assertEquals(currency.getNum(), 400);
        assertEquals(currency.getDecimalPoint(), 3);
        assertEquals(currency.getCurrencyName().trim(), "Jordanian dinar");
        assertEquals(currency.getLocation(), "Jordan");
    }


}
