package com.progressoft.jip.mamoun.dao.imp;

import com.progressoft.jip.mamoun.dao.SessionDao;
import com.progressoft.jip.mamoun.entity.UserEntity;
import com.progressoft.jip.mamoun.entity.UserSessionEntity;
import com.progressoft.jip.mamoun.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SessionDaoImplTest {

    private static final Session session = HibernateUtil.getSessionFactory().openSession();

    private final SessionDao sessionDao;

    private long VALID_ID = 0;

    private UserEntity VALID_USER_ENTITY;

    public SessionDaoImplTest() {
        sessionDao = new SessionDaoImpl();

    }

    @BeforeEach
    public void setUp() {
        clearDB();
        addData();
    }

    @Test
    public void givenNullSessionOrUserEntity_whenSaving_thenIllegalArgumentException() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> sessionDao.saveSession(null));
        assertEquals("'User Session' and 'User' must not be null", exception.getMessage());
    }

    @Test
    public void givenInvalidId_whenCallingDeleteOrFind_thenReturnNull() {
        assertNull(sessionDao.findSession(150));
        assertNull(sessionDao.deleteSession(120));
    }

    @Test
    public void givenNullSessionOrUser_whenUpdating_thenIllegalArgumentException() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> sessionDao.updateSession(null));
        assertEquals("'User Session' and 'User' must not be null", exception.getMessage());
    }

    @Test
    public void givenNewValidNotSavedSession_whenCallingUpdate_thenIllegalArgumentException() {
        UserSessionEntity userSession = new UserSessionEntity();
        userSession.setUserID(VALID_USER_ENTITY.getId());
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> sessionDao.updateSession(userSession));
        assertEquals(exception.getMessage(), userSession + "Could not be found in the database to update it!, are you sure you are calling the right method");
    }

    @Test
    public void givenValidId_whenCallingFindAndDelete_thenReturnSession() {
        UserSessionEntity foundUser = sessionDao.findSession(VALID_ID);
        UserSessionEntity deletedUser = sessionDao.deleteSession(VALID_ID);
        assertEquals(foundUser, deletedUser);
        assertNotNull(foundUser);
        assertEquals(session.createQuery("from UserSessionEntity ").list().size(), 0);
    }


    @Test
    public void givenValidSession_whenUpdating_thenUpdatingAndReturningTheUpdatedObject() {
        UserSessionEntity userSession = sessionDao.findSession(VALID_ID);
        UserEntity user = new UserEntity();
        user.setUsername("Msh Faisal");
        user.setPassword("P@ssw0rd");
        Transaction transaction = session.beginTransaction();
        session.save(user);
        transaction.commit();
        userSession.setUserID(user.getId());
        UserSessionEntity updateSession = sessionDao.updateSession(userSession);
        assertEquals(userSession, updateSession);
        assertNotNull(updateSession);
    }

    @Test
    public void givenValidSession_whenSaving_thenSaveAndReturningSavedSession() {
        UserSessionEntity userSession = new UserSessionEntity();
        userSession.setUserID(VALID_USER_ENTITY.getId());
        UserSessionEntity userSession1 = sessionDao.saveSession(userSession);
        assertEquals(userSession1, userSession);
        assertNotNull(userSession);
        assertNotNull(userSession.getId());
    }

    @Test
    public void whenCallingGetSession_thenReturningArrayListContainingSession() {
        assertNotNull(sessionDao.getSessions());
        assertEquals(sessionDao.getSessions().size(), 1);
    }


    @SuppressWarnings("unchecked")
    private void clearDB() {
        Transaction transaction = session.beginTransaction();
        session.createQuery("delete from UserSessionEntity ").executeUpdate();
        session.createQuery("delete from UserEntity ").executeUpdate();
        transaction.commit();
        List<UserSessionEntity> userEntity = session.createQuery("from UserSessionEntity").list();
        assertEquals(userEntity.size(), 0);
        List<UserEntity> users = session.createQuery("from UserEntity ").list();
        assertEquals(users.size(), 0);

    }

    private void addData() {
        Transaction transaction = session.beginTransaction();
        UserSessionEntity userSession = new UserSessionEntity();
        UserEntity user = new UserEntity();
        user.setUsername("Faisal");
        user.setPassword("P@ssw0rd");
        session.save(user);
        VALID_USER_ENTITY = user;
        userSession.setUserID(user.getId());
        session.save(userSession);
        VALID_ID = userSession.getId();
        transaction.commit();
        assertEquals(session.createQuery("from UserSessionEntity").list().size(), 1);
    }
}