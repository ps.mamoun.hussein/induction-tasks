package com.progressoft.jip.mamoun.dao.imp;

import com.progressoft.jip.mamoun.Currency;
import com.progressoft.jip.mamoun.dao.CurrencyDao;
import org.apache.commons.collections4.list.UnmodifiableList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CurrencyDaoImpTest {

    private final CurrencyDao currencyDao = new CurrencyDaoImp();


    @Test
    public void givenNullCurrencyCode_whenCallingGetCurrencyByCodeOrGetDecimal_thenIllegalArgumentException() {
        String message = "Currency Code must not be null or empty";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> currencyDao.getCurrencyByCode(null));
        assertEquals(message, exception.getMessage());
        exception = assertThrows(IllegalArgumentException.class,
                () -> currencyDao.getDecimal(null));
        assertEquals(message, exception.getMessage());
    }

    @Test
    public void givenValidCode_whenCallingGetCurrencyByCode_thenHappyScenario() {
        Currency usd = currencyDao.getCurrencyByCode("USD");
        assertNotNull(usd);
        assertEquals(usd.getCurrencyCode(), "USD");
        assertEquals(usd.getDecimalPoint(), 2);
        assertEquals(usd.getCurrencyName(), "United States dollar");
        Currency jod = currencyDao.getCurrencyByCode("JOD");
        assertNotNull(jod);
        assertNotEquals(usd, jod);
    }


    @Test
    public void whenCallingGetAllCurrencies_thenReturnListContainingAllCurrency() {
        List<Currency> currencies = currencyDao.getAllCurrencies();
        assertNotNull(currencies);
        assertTrue(currencies.size() > 0);
        assertNotNull(currencies.get(0));
    }

}