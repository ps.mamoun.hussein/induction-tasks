package com.progressoft.jip.mamoun.dao.imp;

import com.progressoft.jip.mamoun.dao.ActivityDao;
import com.progressoft.jip.mamoun.entity.ActivityEntity;
import com.progressoft.jip.mamoun.entity.UserEntity;
import com.progressoft.jip.mamoun.entity.UserSessionEntity;
import com.progressoft.jip.mamoun.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ActivityDaoImplTest {


    private static final Session session = HibernateUtil.getSessionFactory().openSession();

    private Long VALID_ID = 0L;

    private UserSessionEntity VALID_SESSION;

    private final ActivityDao activityDao;

    public ActivityDaoImplTest() {
        this.activityDao = new ActivityDaoImpl();
    }

    @BeforeEach
    public void setUp() {
        clearDB();
        insertData();
    }

    @Test
    public void givenNullActivityOrNullSession_whenSaving_thenIllegalArgumentException() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> activityDao.saveActivity(null));
        assertEquals(exception.getMessage(), "Activity or activity session must not be null");
        ActivityEntity activity = new ActivityEntity();
    }

    @Test
    public void givenInvalidId_whenCallingDeleteOrFind_thenReturningNull() {
        assertNull(activityDao.findActivity(150));
        assertNull(activityDao.deleteActivity(120));
    }

    @Test
    public void givenNullActivityOrNullSession_whenCallingUpdate_thenIllegalArgumentException() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> activityDao.updateActivity(null));
        assertEquals(exception.getMessage(), "Activity or activity session must not be null");
        ActivityEntity activity = activityDao.findActivity(VALID_ID);
    }

    @Test
    public void givenNewNotSavedValidActivity_whenUpdating_thenIllegalArgumentException() {
        ActivityEntity activity = new ActivityEntity();
        activity.setSessionID(VALID_SESSION.getId());
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> activityDao.updateActivity(activity));
        assertEquals(exception.getMessage(), activity + "Could not be found in the database to update it!, are you sure you are calling the right method");
    }

    @Test
    public void givenValidId_whenCallingFindAndDelete_thenReturnActivity() {
        ActivityEntity found = activityDao.findActivity(VALID_ID);
        ActivityEntity deleted = activityDao.deleteActivity(VALID_ID);
        assertEquals(found, deleted);
        assertNotNull(found);
        assertEquals(session.createQuery("from ActivityEntity ").list().size(), 0);
    }

    @Test
    public void givenValidActivity_whenUpdating_thenUpdatingAndReturningTheUpdatedObject() {
        ActivityEntity activity = activityDao.findActivity(VALID_ID);
        UserEntity user = new UserEntity();
        user.setUsername("Msh Osama");
        user.setPassword("P@ssw0rd");
        UserSessionEntity sessionEntity = new UserSessionEntity();
        Transaction transaction = session.beginTransaction();
        session.save(user);
        sessionEntity.setUserID(user.getId());
        session.save(sessionEntity);
        transaction.commit();
        ActivityEntity updateActivity = activityDao.updateActivity(activity);
        assertEquals(activity, updateActivity);
        assertEquals(updateActivity.getSessionID(), activity.getSessionID());
        assertNotNull(updateActivity);
    }

    @Test
    public void givenValidActivity_whenSaving_thenSaveAndReturningSavedSession() {
        ActivityEntity activity = new ActivityEntity();
        activity.setSessionID(VALID_SESSION.getId());
        ActivityEntity activity1 = activityDao.saveActivity(activity);
        assertEquals(activity1, activity);
        assertNotNull(activity1);
    }

    @Test
    public void whenCallingGetSession_thenReturningArrayListContainingSession() {
        assertNotNull(activityDao.getActivity());
        assertEquals(activityDao.getActivity().size(), 1);
    }


    private void insertData() {
        ActivityEntity activity = new ActivityEntity();
        activity.setSourceName("Source");
        activity.setTargetName("Target");
        UserEntity user = new UserEntity();
        user.setUsername("Mamoun");
        user.setPassword("P@ssw0rd");
        session.save(user);
        UserSessionEntity userSession = new UserSessionEntity();
        userSession.setUserID(user.getId());
        session.save(userSession);
        activity.setSessionID(userSession.getId());
        Transaction transaction = session.beginTransaction();
        session.save(activity);
        VALID_ID = activity.getId();
        VALID_SESSION = userSession;
        transaction.commit();
        assertEquals(session.createQuery("from ActivityEntity ").list().size(), 1);

    }

    @SuppressWarnings("unchecked")
    private void clearDB() {
        Transaction transaction = session.beginTransaction();
        session.createQuery("delete from ActivityEntity").executeUpdate();
        session.createQuery("delete from UserSessionEntity ").executeUpdate();
        session.createQuery("delete from UserEntity ").executeUpdate();
        transaction.commit();
        List<UserEntity> userEntity = session.createQuery("from ActivityEntity").list();
        assertEquals(userEntity.size(), 0);
    }


}