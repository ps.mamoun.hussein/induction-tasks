package com.progressoft.jip.mamoun.controller.api.v1;

import com.google.gson.Gson;
import com.progressoft.jip.mamoun.util.FactoryUtility;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SupportedExtensionRestController extends HttpServlet {

    private final Gson gson;

    public SupportedExtensionRestController() {
        gson = new Gson();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String type = req.getParameter("type");
        writeResponse(res, type);
    }

    private void writeResponse(HttpServletResponse res, String type) throws IOException {
        res.setContentType("application/json");
        String fileType = "";
        switch (type) {
            case "read":
                fileType = "supported_read_extensions";
                break;
            case "write":
                fileType = "supported_write_extensions";
        }
        gson.toJson(FactoryUtility.getSupportedExtension(fileType), res.getWriter());
    }

}
