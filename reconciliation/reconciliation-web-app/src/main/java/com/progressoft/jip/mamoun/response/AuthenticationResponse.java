package com.progressoft.jip.mamoun.response;

import java.util.Date;

public class AuthenticationResponse {
    private int status;
    private String message;
    private Date time;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
