package com.progressoft.jip.mamoun.controller;

import com.google.gson.Gson;
import com.progressoft.jip.mamoun.response.LogoutResponse;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LogOutController extends HttpServlet {

    private final Gson gson;

    public LogOutController() {
        this.gson = new Gson();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        writeLogoutResponse(res, session);
    }

    private void writeLogoutResponse(HttpServletResponse res, HttpSession session) throws IOException {
        LogoutResponse logoutResponse = new LogoutResponse();
        if (session != null) {
            session.invalidate();
            logoutResponse.setStatus(HttpServletResponse.SC_ACCEPTED);
            logoutResponse.setMessage("Logged out successful");
        } else {
            logoutResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            logoutResponse.setMessage("You are not logged in");
        }
        gson.toJson(logoutResponse, res.getWriter());
    }
}
