package com.progressoft.jip.mamoun.controller.api.v1;

import com.google.gson.Gson;
import com.progressoft.jip.mamoun.dao.UserDao;
import com.progressoft.jip.mamoun.entity.UserEntity;
import com.progressoft.jip.mamoun.entity.UserSessionEntity;
import com.progressoft.jip.mamoun.response.HistoryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class HistoryRestController extends HttpServlet {


    private final Gson gson;
    private final Logger logger;

    private final UserDao userDao;

    public HistoryRestController(UserDao userDao) {
        gson = new Gson();
        logger = LoggerFactory.getLogger(HistoryRestController.class);
        this.userDao = userDao;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        res.setContentType("application/json");
        HttpSession session = req.getSession(false);
        long userId = (long) session.getAttribute("userID");
        UserEntity user = userDao.findUser(userId);
        HistoryResponse response = new HistoryResponse();
        List<UserSessionEntity> userSessionEntityList = user.getSessions();
        userSessionEntityList.sort(Comparator.comparing(UserSessionEntity::getId));
        response.setSessions(userSessionEntityList);
        response.setStatus(HttpServletResponse.SC_ACCEPTED);
        res.setStatus(HttpServletResponse.SC_ACCEPTED);
        gson.toJson(response, res.getWriter());
    }
}
