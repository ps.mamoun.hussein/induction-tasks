package com.progressoft.jip.mamoun.initializer;

import com.progressoft.jip.mamoun.Filter.AuthFilter;
import com.progressoft.jip.mamoun.controller.ActivityController;
import com.progressoft.jip.mamoun.controller.WelcomeController;
import com.progressoft.jip.mamoun.controller.api.v1.DownloadController;
import com.progressoft.jip.mamoun.controller.api.v1.HistoryRestController;
import com.progressoft.jip.mamoun.controller.api.v1.ReconciliationController;
import com.progressoft.jip.mamoun.controller.LogOutController;
import com.progressoft.jip.mamoun.controller.LoginController;
import com.progressoft.jip.mamoun.controller.api.v1.SupportedExtensionRestController;
import com.progressoft.jip.mamoun.dao.ActivityDao;
import com.progressoft.jip.mamoun.dao.SessionDao;
import com.progressoft.jip.mamoun.dao.UserDao;
import com.progressoft.jip.mamoun.dao.imp.ActivityDaoImpl;
import com.progressoft.jip.mamoun.dao.imp.SessionDaoImpl;
import com.progressoft.jip.mamoun.dao.imp.UserDaoImp;
import com.progressoft.jip.mamoun.util.InitUser;

import javax.servlet.*;
import java.io.IOException;
import java.nio.file.Files;
import java.util.EnumSet;
import java.util.Set;

import static com.progressoft.jip.mamoun.util.Constant.*;

public class ServletInit implements ServletContainerInitializer {
    private final UserDao userDao;
    private final SessionDao sessionDao;

    private final ActivityDao activityDao;

    public ServletInit() {
        InitUser.initUsers();
        userDao = new UserDaoImp();
        sessionDao = new SessionDaoImpl();
        activityDao = new ActivityDaoImpl();
    }

    @Override
    public void onStartup(Set<Class<?>> set, ServletContext servletContext) throws ServletException {
        setCookieConfig(servletContext);
        addAuthFilter(servletContext);
        addWelcomeMapping(servletContext);
        addLoginUrlMapping(servletContext);
        addLogoutMapping(servletContext);
        addFileMapping(servletContext);
        addHistoryMapping(servletContext);
        addSupportedExtensionMapping(servletContext);
        addDownloadMapping(servletContext);
        addActivityController(servletContext);
    }

    private void addActivityController(ServletContext servletContext) {
        ServletRegistration.Dynamic activity = servletContext.addServlet("activity", new ActivityController());
        activity.addMapping("/activity");
    }

    private void addDownloadMapping(ServletContext servletContext) {
        ServletRegistration.Dynamic download = servletContext.addServlet("download", new DownloadController());
        download.addMapping("/api/v1/download");
    }


    private void setCookieConfig(ServletContext servletContext) {
        SessionCookieConfig sessionCookieConfig = servletContext.getSessionCookieConfig();
        sessionCookieConfig.setMaxAge(SESSION_ACTIVE_TIME_IN_SECONDS);
        sessionCookieConfig.setSecure(false);
    }

    private void addAuthFilter(ServletContext servletContext) {
        AuthFilter authFilter = new AuthFilter();
        FilterRegistration.Dynamic authFilterRegistration = servletContext.addFilter("authFilter", authFilter);
        authFilterRegistration.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "*");
    }

    private void addWelcomeMapping(ServletContext servletContext) {
        ServletRegistration.Dynamic welcomeRegistration = servletContext.addServlet("welcome", new WelcomeController());
        welcomeRegistration.addMapping("/welcome");
    }

    private void addLoginUrlMapping(ServletContext servletContext) {
        LoginController loginController = new LoginController(userDao, sessionDao);
        ServletRegistration.Dynamic loginRegistration = servletContext.addServlet("login", loginController);
        loginRegistration.addMapping("/login");
    }

    private void addLogoutMapping(ServletContext servletContext) {
        LogOutController logOutController = new LogOutController();
        ServletRegistration.Dynamic logoutRegistration = servletContext.addServlet("logout", logOutController);
        logoutRegistration.addMapping("/logout");
    }

    private void addFileMapping(ServletContext servletContext) {
        createTempDir();
        ReconciliationController reconciliationController = new ReconciliationController(activityDao, sessionDao);
        MultipartConfigElement multipartConfigElement = new MultipartConfigElement(TEMP_DIR.toString(), MAX_FILE_SIZE, MAX_REQUEST_SIZE, FILE_SIZE_THRESH_HOLD);
        ServletRegistration.Dynamic reconciliationRegistration = servletContext.addServlet("reconciliation", reconciliationController);
        reconciliationRegistration.setMultipartConfig(multipartConfigElement);
        reconciliationRegistration.addMapping("/api/v1/reconciliation");
    }

    private void addHistoryMapping(ServletContext servletContext) {
        HistoryRestController history = new HistoryRestController(userDao);
        ServletRegistration.Dynamic historyRegistration = servletContext.addServlet("history", history);
        historyRegistration.addMapping("/api/v1/history");
    }

    private void addSupportedExtensionMapping(ServletContext servletContext) {
        SupportedExtensionRestController extensionRestController = new SupportedExtensionRestController();
        ServletRegistration.Dynamic extensionRegistration = servletContext.addServlet("supportedExtension", extensionRestController);
        extensionRegistration.addMapping("/api/v1/extension");
    }

    private void createTempDir() {
        if (Files.notExists(TEMP_DIR)) {
            try {
                Files.createDirectory(TEMP_DIR);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
