package com.progressoft.jip.mamoun.controller.api.v1;

import com.progressoft.jip.mamoun.GeneralMissingModel;
import com.progressoft.jip.mamoun.GeneralModel;
import com.progressoft.jip.mamoun.writer.ReconciliationWriter;
import com.progressoft.jip.mamoun.writer.factory.ReconciliationWriterFactory;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class DownloadController extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        String type = req.getParameter("type");
        String ext = req.getParameter("ext");
        resp.setContentType("text/" + ext);
        resp.setHeader("Content-disposition", "attachment; filename=" + type + "." + ext);
        ReconciliationWriter writer = ReconciliationWriterFactory.getWriter(ext);
        switch (type) {
            case "matching":
                downloadMatching(session, resp, type, writer, ext);
                break;
            case "mismatching":
            case "missing":
                downloadMiss(session, resp, type, writer, ext);
                break;
        }

    }

    private void downloadMiss(HttpSession session, HttpServletResponse resp, String type, ReconciliationWriter writer, String ext) throws IOException {
        GeneralMissingModel[] data = (GeneralMissingModel[]) session.getAttribute(type);
        if (type.equals("missing")) writer.writeMissingFile(Arrays.asList(data));
        else writer.writeMisMatchingFile(Arrays.asList(data));
        writeFileResponseDownload(resp, type, writer, ext);
    }

    private void downloadMatching(HttpSession session, HttpServletResponse resp, String type, ReconciliationWriter writer, String ext) throws IOException {
        GeneralModel[] matching = (GeneralModel[]) session.getAttribute("matching");
        writer.writeMatchingFile(Arrays.asList(matching));
        writeFileResponseDownload(resp, type, writer, ext);
    }

    private void writeFileResponseDownload(HttpServletResponse resp, String type, ReconciliationWriter writer, String ext) throws IOException {
        String folder_path = String.valueOf(writer.getFOLDER_PATH());
        try (ServletOutputStream outputStream = resp.getOutputStream(); InputStream inputStream = Files.newInputStream(Paths.get(folder_path, type + "." + ext))) {
            int i;
            while ((i = inputStream.read()) != -1) {
                outputStream.write(i);
            }
            outputStream.flush();
        }
    }


}
