package com.progressoft.jip.mamoun.controller.api.v1;

import com.google.gson.Gson;
import com.progressoft.jip.mamoun.*;
import com.progressoft.jip.mamoun.dao.ActivityDao;
import com.progressoft.jip.mamoun.dao.SessionDao;
import com.progressoft.jip.mamoun.entity.ActivityEntity;
import com.progressoft.jip.mamoun.entity.UserSessionEntity;
import com.progressoft.jip.mamoun.reader.ReconciliationReader;
import com.progressoft.jip.mamoun.reader.factory.ReconciliationReaderFactory;
import com.progressoft.jip.mamoun.response.ReconciliationResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.*;
import java.nio.file.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class ReconciliationController extends HttpServlet {

    private final Logger logger;


    private final ActivityDao activityDao;

    private final SessionDao sessionDao;
    private final Gson gson;

    public ReconciliationController(ActivityDao activityDao, SessionDao sessionDao) {
        logger = LoggerFactory.getLogger(ReconciliationController.class);

        this.activityDao = activityDao;
        this.sessionDao = sessionDao;
        gson = new Gson();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
//        res.setContentType("application/json");
        Part source = req.getPart("source");
        Part target = req.getPart("target");
        String sourceName = req.getParameter("sourceName");
        String targetName = req.getParameter("targetName");
        HttpSession session = req.getSession(false);
        long sessionID = (long) session.getAttribute("sessionID");
        UserSessionEntity userSession = sessionDao.findSession(sessionID);
        if (source != null && target != null) {
            Reconciliation reconciliation = writeSuccessResponse(res, source, target, sourceName, targetName, userSession);
            session.setAttribute("matching", reconciliation.getMatchingTransaction().toArray(new GeneralModel[0]));
            session.setAttribute("mismatching", reconciliation.getMisMatchingTransaction().toArray(new GeneralMissingModel[0]));
            session.setAttribute("missing", reconciliation.getMissingTransaction().toArray(new GeneralMissingModel[0]));
        } else {
            writeFailureResponse(res);

        }


    }

    private void writeFailureResponse(HttpServletResponse res) throws IOException {
        ReconciliationResponse response = new ReconciliationResponse();
        response.setMessage("Missing File");
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        gson.toJson(response, res.getWriter());
        logger.error("Missing files");
    }

    private Reconciliation writeSuccessResponse(HttpServletResponse res, Part source, Part target, String sourceName, String targetName, UserSessionEntity userSession) throws IOException {
        List<GeneralModel> sourceData = getData(source, sourceName);
        List<GeneralModel> targetData = getData(target, targetName);
        logger.info("File Uploaded Successfully");
        Reconciliation reconciliation = new Reconciliation(sourceData, targetData);
        List<GeneralModel> matchingJson = toJson(reconciliation.getMatchingTransaction());
        List<GeneralMissingModel> misMatchingJson = toJsonMissing(reconciliation.getMisMatchingTransaction());
        List<GeneralMissingModel> missingJson = toJsonMissing(reconciliation.getMissingTransaction());
        ReconciliationResponse response = new ReconciliationResponse(matchingJson, misMatchingJson, missingJson);
        response.setStatus(HttpServletResponse.SC_ACCEPTED);
        response.setMessage("Success");
        res.setStatus(HttpServletResponse.SC_ACCEPTED);
        ActivityEntity activity = new ActivityEntity();
        activity.setSessionID(userSession.getId());
        activity.setSourceName(sourceName != null ? sourceName : source.getSubmittedFileName());
        activity.setTargetName(targetName != null ? targetName : target.getSubmittedFileName());
        activity.setMatched(matchingJson.size());
        activity.setMismatched(misMatchingJson.size());
        activity.setMissing(missingJson.size());
        activityDao.saveActivity(activity);
        sessionDao.updateSession(userSession);
        gson.toJson(response, res.getWriter());
        return reconciliation;
    }

    private List<GeneralMissingModel> toJsonMissing(List<GeneralMissingModel> misMatchingTransaction) {
        List<GeneralMissingModel> misMatchingJson = new ArrayList<>();
        for (GeneralMissingModel generalMissingModel : misMatchingTransaction) {
            Json json = getJson(generalMissingModel.getGeneralModel());
            misMatchingJson.add(new MissingModel(json, generalMissingModel.getFileSource()));
        }
        return misMatchingJson;
    }

    private List<GeneralModel> toJson(List<GeneralModel> matchingTransaction) {
        List<GeneralModel> matchingJson = new ArrayList<>();
        for (GeneralModel generalModel : matchingTransaction) {
            Json json = getJson(generalModel);
            matchingJson.add(json);
        }
        return matchingJson;
    }

    private Json getJson(GeneralModel generalModel) {
        Json json = new Json();
        json.setAmount(generalModel.getAmount().toString());
        json.setCurrencyCode(generalModel.getCurrencyCode());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        json.setDate(simpleDateFormat.format(generalModel.getValueDate()));
        json.setReference(generalModel.getTransactionId());
        return json;
    }

    private List<GeneralModel> getData(Part part, String fileName) throws IOException {
        String autoGeneratedFileName = LocalDateTime.now() + part.getSubmittedFileName();
        InputStream inputStream = part.getInputStream();
        Path path = Paths.get(autoGeneratedFileName);
        Files.copy(inputStream, path, StandardCopyOption.REPLACE_EXISTING);
        ReconciliationReader reader = ReconciliationReaderFactory.getReader(path);
        List<GeneralModel> data = reader.getReconciliationData();
        logger.info(path.toAbsolutePath().toString());
        return data;
    }
}
