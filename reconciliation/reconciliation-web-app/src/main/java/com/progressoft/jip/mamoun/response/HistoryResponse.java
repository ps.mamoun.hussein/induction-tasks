package com.progressoft.jip.mamoun.response;

import com.progressoft.jip.mamoun.entity.UserSessionEntity;

import java.util.List;

public class HistoryResponse {

    private int status;

    private String message;

    private List<UserSessionEntity> sessions;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<UserSessionEntity> getSessions() {
        return sessions;
    }

    public void setSessions(List<UserSessionEntity> sessions) {
        this.sessions = sessions;
    }
}
