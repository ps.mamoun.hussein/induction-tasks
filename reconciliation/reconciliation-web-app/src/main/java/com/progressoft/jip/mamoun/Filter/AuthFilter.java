package com.progressoft.jip.mamoun.Filter;

import com.google.gson.Gson;
import com.progressoft.jip.mamoun.response.AuthenticationResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;

import static com.progressoft.jip.mamoun.util.Constant.*;


public class AuthFilter implements Filter {
    private Logger logger;
    private Gson gson;


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
        logger = LoggerFactory.getLogger(AuthFilter.class);
        logger.info("Init Filter");
        gson = new Gson();
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse res = (HttpServletResponse) servletResponse;
        res.setContentType("application/json");
        String requestURI = req.getRequestURI();
        String contextPath = req.getContextPath();
        HttpSession session = req.getSession(false);
        filter(servletRequest, servletResponse, filterChain, req, res, requestURI, contextPath, session);


    }

    private void filter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain, HttpServletRequest req, HttpServletResponse res, String requestURI, String contextPath, HttpSession session) throws IOException, ServletException {
        if (isResources(servletRequest, servletResponse, filterChain, requestURI)) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        try {

            userAuth(servletRequest, servletResponse, filterChain, req, res, requestURI, contextPath, session);
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }

    private void userAuth(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain, HttpServletRequest req, HttpServletResponse res, String requestURI, String contextPath, HttpSession session) throws IOException, ServletException {

        if (requestURI.startsWith(contextPath + LOGIN_URL)) {
            if (!isUserLoggedIn(session)) {
                filterChain.doFilter(servletRequest, servletResponse);
                logger.info("went to login");
            } else {
                logger.info("trying to login twice");
//                write(res, session);
                res.setStatus(HttpServletResponse.SC_OK);
                res.sendRedirect(req.getContextPath() + "/");
            }
        } else {
            if (isUserLoggedIn(session)) {
                filterChain.doFilter(servletRequest, servletResponse);
                logger.info("user authenticated");
            } else {
                logger.info("user un authorized");
                res.sendRedirect(req.getContextPath() + "/login");
            }
        }

    }

    private boolean isResources(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain, String requestURI) throws IOException, ServletException {
        if (requestURI.endsWith(".css") || requestURI.endsWith(".js")) {
            filterChain.doFilter(servletRequest, servletResponse);
            return true;
        }
        return false;
    }

    private void write(HttpServletResponse res, HttpSession session) throws IOException {
        AuthenticationResponse authenticationResponse = new AuthenticationResponse();
        authenticationResponse.setMessage("User already authorised");
        authenticationResponse.setStatus(HttpServletResponse.SC_CONFLICT);
        res.setStatus(HttpServletResponse.SC_CONFLICT);
        gson.toJson(authenticationResponse, res.getWriter());
    }

    private boolean isUserLoggedIn(HttpSession session) {
        return session != null && session.getAttribute("userID") != null;
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }


}
