package com.progressoft.jip.mamoun.response;

import com.progressoft.jip.mamoun.GeneralMissingModel;
import com.progressoft.jip.mamoun.GeneralModel;

import java.util.List;

public class ReconciliationResponse {

    private int status;


    private String message;

    private List<GeneralModel> matching;

    private List<GeneralMissingModel> mismatching;

    private List<GeneralMissingModel> missing;

    public ReconciliationResponse() {
    }

    public ReconciliationResponse(List<GeneralModel> matching, List<GeneralMissingModel> mismatching, List<GeneralMissingModel> missing) {
        this.matching = matching;
        this.mismatching = mismatching;
        this.missing = missing;
    }


    public List<GeneralModel> getMatching() {
        return matching;
    }

    public void setMatching(List<GeneralModel> matching) {
        this.matching = matching;
    }

    public List<GeneralMissingModel> getMismatching() {
        return mismatching;
    }

    public void setMismatching(List<GeneralMissingModel> mismatching) {
        this.mismatching = mismatching;
    }

    public List<GeneralMissingModel> getMissing() {
        return missing;
    }

    public void setMissing(List<GeneralMissingModel> missing) {
        this.missing = missing;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
