package com.progressoft.jip.mamoun.util;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Constant {


    public static final int SESSION_ACTIVE_TIME_IN_SECONDS = 10 * 60;

    public static final String LOGIN_URL = "/login";

    public static final Path TEMP_DIR = Paths.get("file-upload-servlet").toAbsolutePath();

    public static final long MAX_FILE_SIZE = 50 * 1024;

    public static final long MAX_REQUEST_SIZE = 1024 * 1024 * 100;

    public static final int FILE_SIZE_THRESH_HOLD = 1024 * 1024;


}

