package com.progressoft.jip.mamoun.controller;

import com.google.gson.Gson;
import com.progressoft.jip.mamoun.dao.SessionDao;
import com.progressoft.jip.mamoun.dao.UserDao;
import com.progressoft.jip.mamoun.entity.UserEntity;
import com.progressoft.jip.mamoun.entity.UserSessionEntity;
import com.progressoft.jip.mamoun.response.AuthenticationResponse;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Date;

import static com.progressoft.jip.mamoun.util.Constant.LOGIN_URL;
import static com.progressoft.jip.mamoun.util.Constant.SESSION_ACTIVE_TIME_IN_SECONDS;
import static com.progressoft.jip.mamoun.util.PasswordUtil.isPasswordMatching;

public class LoginController extends HttpServlet {


    public static final String LOGIN_JSP = "/view/page/login.jsp";
    private final UserDao userDao;
    private final SessionDao sessionDao;

    private final Gson gson;

    public LoginController(UserDao userDao, SessionDao sessionDao) {
        this.userDao = userDao;
        this.sessionDao = sessionDao;
        this.gson = new Gson();
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher(LOGIN_JSP);
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        UserEntity user = getUser(username);
        if (user == null) {
            sendLoginError(req, res);
            return;
        }
        if (!isPasswordMatching(password, user.getPassword())) {
            sendLoginError(req, res);
            return;
        }
        login(req, res, user);
    }

    private UserEntity getUser(String username) {
        return userDao.findUser(username);
    }

    private void sendLoginError(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        AuthenticationResponse authenticationResponse = new AuthenticationResponse();
        authenticationResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        authenticationResponse.setMessage("Incorrect username or password");
        gson.toJson(authenticationResponse, resp.getWriter());
    }

    private void login(HttpServletRequest req, HttpServletResponse resp, UserEntity user) throws IOException {
        HttpSession session = setSessionProprieties(req);
        setUserSession(user, session);
        writeResponse(req, resp, session);
    }

    private HttpSession setSessionProprieties(HttpServletRequest req) {
        req.getSession().invalidate();
        return req.getSession(true);
    }

    private void setUserSession(UserEntity user, HttpSession session) {
        UserSessionEntity userSession = new UserSessionEntity();
        userSession.setUserID(user.getId());
        userSession.setTime(new Date(session.getCreationTime()));
        sessionDao.saveSession(userSession);
        session.setAttribute("sessionID", userSession.getId());
        session.setAttribute("userID", user.getId());
    }

    private void writeResponse(HttpServletRequest req, HttpServletResponse resp, HttpSession session) throws IOException {
//        resp.setStatus(HttpServletResponse.SC_ACCEPTED);
//        resp.sendRedirect(req.getContextPath() + "/");
        resp.setStatus(HttpServletResponse.SC_OK);
        long creationTime = session.getCreationTime();
        AuthenticationResponse authenticationResponse = new AuthenticationResponse();
        authenticationResponse.setMessage("Your session is active until");
        authenticationResponse.setTime(new Date(creationTime + SESSION_ACTIVE_TIME_IN_SECONDS * 1000));
        authenticationResponse.setStatus(HttpServletResponse.SC_OK);
        gson.toJson(authenticationResponse, resp.getWriter());

    }


}
