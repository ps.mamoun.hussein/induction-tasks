<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="tag"
          tagdir="/WEB-INF/tags" %>
<html>
<tag:head style="activity.css"/>
<body>
<tag:header/>
<main>
    <table id="bigTable">
        <thead>
        <tr>
            <th>
                SessionTime
            </th>
            <th>
                Reconciliation Activities
            </th>
        </tr>
        </thead>
    </table>
</main>
<tag:footer js="activity.js"/>
</body>
</html>
