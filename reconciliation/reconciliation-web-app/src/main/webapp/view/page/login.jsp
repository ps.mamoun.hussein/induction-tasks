<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="tag"
          tagdir="/WEB-INF/tags" %>

<html>
<tag:head style="login.css"/>
<body>
<header>

</header>
<main>
    <form id="loginForm" method="post">
        <label for="username">
            Username
            <input
                    id="username"
                    type="text"
                    name="username"
                    accept="text/strings"
                    required
            />
        </label>
        <label for="password">
            Password
            <input
                    id="password"
                    type="password"
                    name="password"
                    accept="text/strings"
                    required
            />
        </label>
        <p id="error"></p>
        <div id="button-cont">
            <button type="submit">Login</button>
        </div>
    </form>
</main>
<tag:footer js="login.js"/>
</body>
</html>
