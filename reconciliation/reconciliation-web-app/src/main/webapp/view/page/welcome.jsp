<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="tag"
          tagdir="/WEB-INF/tags" %>
<html>
<tag:head style="welcome.css"/>
<body>
<tag:header/>
<main>
    <form id="sourceForm">
        <label for="sourceName">
            Source Name
            <input type="text" id="sourceName" name="sourceName" placeholder="">
        </label>
        <label for="uploadSourceFileType">
            File Type
            <select class="fileType" id="uploadSourceFileType"></select>
        </label>

        <div class="upload-cont">
            <input type="file" id="hiddenSourceFile" accept="application/json, text/csv" name="sourceFile" hidden/>
            <label>
                File:
                <input id="uploadedSourceName" type="text" disabled>
            </label>
            <button type="button" id="sourceBrowseButton" class="browse-button">
                Browse
            </button>
        </div>
        <div class="next-cont">
            <button id="sourceNext" type="submit">
                Next
            </button>
        </div>

    </form>
    <form id="targetForm" style="display: none">
        <label for="targetName">
            Target Name
            <input type="text" id="targetName" name="targetName">
        </label>
        <label>
            File Type
            <select class="fileType" id="uploadTargetFileType"></select>
        </label>
        <div class="upload-cont">
            <input type="file" id="hiddenTargetFile" accept="application/json, text/csv" name="targetFile" hidden/>
            <label>
                File:
                <input id="uploadedTargetName" type="text" disabled>
            </label>
            <button type="button" id="targetBrowseButton" class="browse-button">
                Browse
            </button>
        </div>
        <div class="next-cont">
            <button type="button" id="targetPrev">
                Previous
            </button>
            <button type="submit" id="targetNext">
                Next
            </button>
        </div>
    </form>
    <div id="finalCompare" style="display: none">
        <div id="file-cont">
            <div class="file-cont" id="source-cont">
                <h2>Source</h2>
                <p id="srcName"></p>
                <p id="srcType"></p>
            </div>
            <div class="file-cont" id="target-cont">
                <h2>Target</h2>
                <p id="trName"></p>
                <p id="trType"></p>
            </div>
        </div>
        <label>
            Result Files Format:
            <select id="writeExtension">
            </select>
        </label>
        <div id="button-cont">
            <button id="cancel">
                Cancel
            </button>
            <div id="right-button-cont">
                <button id="back">Back</button>
                <button id="compare">Compare</button>
            </div>
        </div>
    </div>
    <div id="resultTable" hidden>
        <div id=tableSelector>
            <p id="matchingTap">
                Matching
            </p>
            <p id="mismatchingTap">
                Mismatching
            </p>
            <p id="missingTap">
                Missing
            </p>
        </div>
        <table id="matchingTable"></table>
        <table id="mismatchingTable" style="display: none"></table>
        <table id="missingTable" style="display: none"></table>
        <div class="dropdown">
            <button onclick="myFunction()" class="dropbtn">Dropdown</button>
            <div id="myDropdown" class="dropdown-content"></div>
        </div>

    </div>
</main>
<tag:footer js="welcome.js"/>
<script src="${pageContext.request.contextPath}/view/js/table.js">

</script>
</body>
</html>
