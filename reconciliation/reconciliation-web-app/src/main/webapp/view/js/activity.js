const bigTable = document.getElementById("bigTable");
let qqqq = []

const header = ["#", "Time", "source", "Target", "Matched", "Mismatched", "Missing"]


function createTable(res) {
    let tbody = document.createElement("tbody");
    console.log(res)
    res.forEach((data) => {
        console.log(data)
        let tr = document.createElement("tr")
        let td = document.createElement("td");
        td.innerHTML = data.time
        tr.appendChild(td);
        let htmlTableElement = createSmallTable(data);
        tr.appendChild(htmlTableElement)
        tbody.appendChild(tr);
    })
    bigTable.appendChild(tbody);
}

const fetchHistory = () => {
    $.ajax("api/v1/history").done(res => {
        qqqq = res
        createTable(res.sessions)
    }).fail(error => console.log(error));
}

fetchHistory();


const createSmallTable = (data) => {
    let table = document.createElement("table")
    let thead = document.createElement("thead")
    let tbody = document.createElement("tbody")
    let tr = document.createElement("tr");
    header.forEach((head) => {
        let th = document.createElement("th");
        th.innerHTML = head
        tr.appendChild(th);
    })
    thead.appendChild(tr);
    table.appendChild(thead)
    console.log(data)
    if (data.activityEntities.length === 0) {
        let tr = document.createElement("tr");
        let td = document.createElement("td");
        td.innerHTML = "No Activities"
        td.className = "empty"
        td.colSpan = header.length
        tr.appendChild(td);
        tbody.appendChild(tr);
        // tbody.innerHTML = "No Activities"
    } else {
        data.activityEntities.forEach((entities, index) => {
            // console.log(entities)
            // activityEntities.forEach((activity, index) => {
            let row = document.createElement("tr")
            let indexData = document.createElement("td")
            indexData.innerHTML = index;
            row.appendChild(indexData);
            let time = document.createElement("td")
            time.innerHTML = entities.time;
            row.appendChild(time);
            let source = document.createElement("td")
            source.innerHTML = entities.sourceName;
            row.appendChild(source);
            let target = document.createElement("td")
            target.innerHTML = entities.targetName;
            row.appendChild(target);
            let matched = document.createElement("td")
            matched.innerHTML = entities.matched;
            row.appendChild(matched);
            let mismatched = document.createElement("td")
            mismatched.innerHTML = entities.mismatched;
            row.appendChild(mismatched);
            let missing = document.createElement("td")
            missing.innerHTML = entities.missing;
            row.appendChild(missing);
            tbody.appendChild(row);
        })

        // })
    }

    table.appendChild(tbody)
    table.className = "small-table"
    return table;
}