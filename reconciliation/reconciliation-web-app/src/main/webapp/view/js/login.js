const form = document.getElementById("loginForm");

form.addEventListener("submit", (e) => {
    e.preventDefault();
    const sendData = {
        username: $("#username").val(),
        password: $("#password").val(),
    };
    $.post("login", sendData)
        .done(() => {
            window.location = "welcome";
        })
        .fail((error) => {
            console.log(error);
            $("#error").text(error.responseJSON.message);
        });
});