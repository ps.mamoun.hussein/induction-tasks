const sourceName = document.getElementById("sourceName")
const targetName = document.getElementById("targetName");

const sourceForm = document.getElementById("sourceForm");
const targetForm = document.getElementById("targetForm");

const hiddenSourceFile = document.getElementById("hiddenSourceFile");
const hiddenTargetFile = document.getElementById("hiddenTargetFile");

const uploadedSourceName = document.getElementById("uploadedSourceName")
const uploadedTargetName = document.getElementById("uploadedTargetName");

const uploadSourceFileType = document.getElementById("uploadSourceFileType");
const uploadTargetFileType = document.getElementById("uploadTargetFileType");
const downloadFileType = document.getElementById("writeExtension");

const sourceBrowseButton = document.getElementById("sourceBrowseButton");
const targetBrowseButton = document.getElementById("targetBrowseButton");

const targetPrev = document.getElementById("targetPrev");

const finalCompare = document.getElementById("finalCompare");

const cancel = document.getElementById("cancel");
const back = document.getElementById("back");



const readExtension = []
const writeExtension = []


let sendSourceFile;
let sendTargetFile;


const configReadFile = () => {
    let types = fetchSelectType("read");
    types.then(data => {
        data.forEach((extension) => {
            readExtension.push(extension)
            let option = document.createElement("option");
            option.innerText = extension.toUpperCase();
            uploadSourceFileType.appendChild(option)
            option = document.createElement("option");
            option.innerText = extension.toUpperCase();
            uploadTargetFileType.appendChild(option)
        })
    })

}

const configWriteType = () => {
    let types = fetchSelectType("write")
    types.then(data => {
        data.forEach((extension) => {
            writeExtension.push(extension)
            let option = document.createElement("option");
            option.innerText = extension.toUpperCase();
            downloadFileType.appendChild(option)
        })
        writeHref(data);
    })
}

const fetchSelectType = async (type) => {
    return $.ajax("api/v1/extension", {
        data: `type=${type}`
    });
}

configReadFile()
configWriteType()

sourceBrowseButton.addEventListener("click", () => {
    hiddenSourceFile.click();
});

targetBrowseButton.addEventListener("click", () => {
    hiddenTargetFile.click();
});

hiddenSourceFile.addEventListener("change", () => {
    selectConfigure(hiddenSourceFile, uploadedSourceName, uploadSourceFileType)
});


hiddenTargetFile.addEventListener("change", () => {
    selectConfigure(hiddenTargetFile, uploadedTargetName, uploadTargetFileType)
});


const selectConfigure = (file, name, selectType) => {
    let fileName = file.files.length > 0 ? file.files[0].name : "";
    name.value = fileName;
    let extension = getExtension(fileName);
    let index = readExtension.indexOf(extension);

    if (index !== null && index !== -1) {
        selectType.options.selectedIndex = index
        selectType.disabled = true
    } else {
        selectType.disabled = false
    }
}


const getExtension = (fileName) => {
    let indexOf = fileName.lastIndexOf(".") + 1;
    if (indexOf < 1) return null
    return fileName.substring(indexOf);
}


const hideForms = () => {
    sourceForm.style.display = "none"
    targetForm.style.display = "none"
    finalCompare.style.display = "none"
}

sourceForm.addEventListener("submit", e => {
    sendSourceFile = handleFileName(sourceName, hiddenSourceFile, uploadSourceFileType)
    handleFormVisibility(e, targetForm, hiddenSourceFile)
})


const handleFormVisibility = (thisForm, shownForm, file) => {
    thisForm.preventDefault();
    if (file.files[0]) {
        hideForms();
        shownForm.style.display = "grid"
    } else {
        alert("Please Select File")
    }

}

targetForm.addEventListener("submit", e => {
    handleFormVisibility(e, finalCompare, hiddenTargetFile)
    sendTargetFile = handleFileName(targetName, hiddenTargetFile, uploadTargetFileType)
    $("#srcName")[0].innerText = sendSourceFile.name
    $("#trName")[0].innerText = sendTargetFile.name
    $("#srcType")[0].innerText = uploadSourceFileType.selectedOptions[0].value
    $("#trType")[0].innerText = uploadTargetFileType.selectedOptions[0].value

})

const handleFileName = (typedName, file, selectOption) => {
    if (typedName.value !== "") {
        file = new File(file.files, `${typedName.value}.${selectOption.selectedOptions[0].value.toLowerCase()}`)
    } else {
        file = file.files[0]
    }
    return file

}

cancel.addEventListener("click", () => {
    window.location = "welcome"
})

back.addEventListener("click", e => {
    handleFormVisibility(e, targetForm, hiddenTargetFile)
})

targetPrev.addEventListener("click", e => handleFormVisibility(e, sourceForm, hiddenSourceFile))



