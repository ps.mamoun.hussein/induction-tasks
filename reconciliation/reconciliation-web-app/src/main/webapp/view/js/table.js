const compare = document.getElementById("compare");
const resultTable = document.getElementById("resultTable");
const myDropdown = document.getElementById("myDropdown");
let type = "matching"
let matching = {};
let mismatching = [];
let missing = [];
const matchingTap = document.getElementById("matchingTap");
const mismatchingTap = document.getElementById("mismatchingTap");
const missingTap = document.getElementById("missingTap");
const matchingTable = document.getElementById("matchingTable");
const mismatchingTable = document.getElementById("mismatchingTable");
const missingTable = document.getElementById("missingTable")

const header = ["#", "Transaction ID", "Amount", "Currency", "Value Date"]


const hideAllTable = (table) => {
    matchingTable.style.display = "none";
    mismatchingTable.style.display = "none";
    missingTable.style.display = "none";
    table.style.display = "table"

};

function handleMarkedTap(tap) {
    matchingTap.style.background = "white"
    mismatchingTap.style.background = "white"
    missingTap.style.background = "white"
    matchingTap.style.color = "black"
    mismatchingTap.style.color = "black"
    missingTap.style.color = "black"



    tap.style.background = "black"
    tap.style.color = "white"
}

matchingTap.addEventListener("click", () => {
    type = "matching"
    hideAllTable(matchingTable)
    handleMarkedTap(matchingTap);

})
mismatchingTap.addEventListener("click", () => {
    type = "mismatching"
    hideAllTable(mismatchingTable)
    handleMarkedTap(mismatchingTap);



})
missingTap.addEventListener("click", () => {
    type = "missing"
    hideAllTable(missingTable)
    handleMarkedTap(missingTap);


})

const writeHref = (data) => {
    data.forEach(ext => {
        let a = document.createElement("a");
        a.id = ext
        a.innerHTML = ext
        myDropdown.appendChild(a)
        a.addEventListener("click", e => {
            a.href = `api/v1/download?type=${type}&ext=${a.id}`
        })
    })
}

function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}


compare.addEventListener("click", e => {
    handleFormVisibility(e, resultTable, hiddenTargetFile)
    let data = new FormData();
    data.append("source", sendSourceFile);
    data.append("target", sendTargetFile);
    $.ajax({
        url: "api/v1/reconciliation", contentType: false, processData: false, data: data, method: "POST"
    }).done((res) => {
        matching = res.matching;
        mismatching = res.mismatching;
        missing = res.missing;
        createTables();
    }).fail((error) => {
        console.log(error);
    });
})


const createTables = () => {
    createMatching();
}


function createHeader(table) {
    let tableHeader = document.createElement("thead")
    let tr = document.createElement("tr")
    header.forEach(head => {
        let th = document.createElement("th")
        th.innerHTML = head
        tr.appendChild(th);
    })
    tableHeader.appendChild(tr)
    table.appendChild(tableHeader);
}

function createTablesHeader() {
    createHeader(matchingTable);
    createHeader(missingTable);
    createHeader(mismatchingTable)
}

function createMatchingBody(previewedData, table) {
    let tbody = document.createElement("tbody");
    previewedData.forEach((data, index) => {
        let tr = document.createElement("tr");
        let td = document.createElement("td")
        tr.appendChild(td);
        td.innerHTML = index;
        for (let key in data) {
            let td = document.createElement("td");
            td.innerHTML = data[key]
            tr.appendChild(td);
        }
        tbody.appendChild(tr);
        table.appendChild(tbody)

    })
}


function createMissingBody(previewedData, table) {
    let tbody = document.createElement("tbody");
    previewedData.forEach((data, index) => {
        let tr = document.createElement("tr");
        let td = document.createElement("td")
        tr.appendChild(td);
        td.innerHTML = index;
        let generalData = data.generalModel
        for (let key in generalData) {
            let td = document.createElement("td");
            td.innerHTML = generalData[key]
            tr.appendChild(td);
        }
        tbody.appendChild(tr);
        table.appendChild(tbody)

    })
}

function createTableBody() {
    createMatchingBody(matching, matchingTable);
    createMissingBody(mismatching, mismatchingTable);
    createMissingBody(missing, missingTable);
}

const createMatching = () => {
    createTablesHeader();
    createTableBody();
    handleMarkedTap(matchingTap)
}
