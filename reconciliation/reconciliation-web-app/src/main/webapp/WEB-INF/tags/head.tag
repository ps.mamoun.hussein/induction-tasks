<%@attribute name="style" required="true" %>

<head>
    <title>Reconciliation</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/view/style/reset.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/view/style/body.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/view/style/header.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/view/style/main.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/view/style/footer.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/view/style/${style}">
    <script src="https://code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

</head>

