<header>
    <script>
        const logout = () => {
            $.post("logout").done(() => {
                window.location = ""
            })
        }
    </script>
    <div>
        <a href="/reconciliation_web_app_war/activity">
            View History
        </a>
        <button onclick="logout()">
            Log out
        </button>
    </div>
</header>