import com.progressoft.jip.mamoun.Filter.AuthFilter;
import org.junit.jupiter.api.Test;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static org.mockito.Mockito.*;


public class AuthFilterTest {

    @Test
    public void givenNoSessionUser_whenLunchingSite_thenRedirect() throws ServletException, IOException {
        AuthFilter authFilter = new AuthFilter();
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);
        HttpSession session = mock(HttpSession.class);
        when(request.getRequestURI()).thenReturn("war/welcome");
        when(request.getContextPath()).thenReturn("war");
        authFilter.init(mock(FilterConfig.class));
        authFilter.doFilter(request, response, filterChain);
        verify(request, atLeast(1)).getSession(false);
        verify(response).sendRedirect("war/login");

    }

    @Test
    public void givenNoSessionUser_whenAccessingResources_thenDoFilter() throws ServletException, IOException {
        AuthFilter authFilter = new AuthFilter();
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);
        when(request.getRequestURI()).thenReturn("war/welcome/style.css");
        when(request.getContextPath()).thenReturn("war");
        authFilter.init(mock(FilterConfig.class));
        authFilter.doFilter(request, response, filterChain);
        verify(request, atLeast(1)).getSession(false);
        verify(filterChain, atLeast(1)).doFilter( request, response);

    }


    @Test
    public void givenLogInUrl_whenEnteringSite_thenDoFilter() throws ServletException, IOException {
        AuthFilter authFilter = new AuthFilter();
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);
        when(request.getRequestURI()).thenReturn("war/login");
        when(request.getContextPath()).thenReturn("war");
        authFilter.init(mock(FilterConfig.class));
        authFilter.doFilter(request, response, filterChain);
        verify(request, atLeast(1)).getSession(false);
        verify(filterChain, atLeast(1)).doFilter(request,response);

    }
}
