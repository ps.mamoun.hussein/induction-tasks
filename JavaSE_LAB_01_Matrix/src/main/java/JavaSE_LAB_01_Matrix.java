import java.util.Arrays;

public class JavaSE_LAB_01_Matrix {
    public static void main(String[] args) {
        double[][] arr1 = {{5, 2, 3}, {6, 2, 1}};
        double[][] arr2 = {{1, 5, 8}, {7, 5, 5}};
        double[][] arr3 = {{2, 3}, {5, 6}, {7, 8}};
        double[][] squaredMatrix = {{2, 5, 7}, {4, 5, 8}, {8, 9, 6}};
        double[][] combined = Matrix.addTwoArray(arr1, arr2);
        double[][] scaledArray = Matrix.scalarProduct(arr1, 5.0);
        double[][] transportationArray = Matrix.transportation(arr1);
        double[][] multipliedArray = Matrix.multiplyTwoArray(arr1, arr3);
        double[][] subArray = Matrix.subMatrix(arr3, 0, 1);
        double[][] diagonal = Matrix.squareMatrix(squaredMatrix, Matrix.DIAGONAL_MATRIX);
        double[][] lowerTriangle = Matrix.squareMatrix(squaredMatrix, Matrix.LOWER_TRIANGLE);
        double[][] upperTriangle = Matrix.squareMatrix(squaredMatrix, Matrix.UPPER_TRIANGLE);
        double det = Matrix.determination(squaredMatrix);


        System.out.println(Arrays.deepToString(combined));
        System.out.println(Arrays.deepToString(scaledArray));
        System.out.println(Arrays.deepToString(transportationArray));
        System.out.println(Arrays.deepToString(multipliedArray));
        System.out.println(Arrays.deepToString(subArray));
        System.out.println(Arrays.deepToString(diagonal));
        System.out.println(Arrays.deepToString(lowerTriangle));
        System.out.println(Arrays.deepToString(upperTriangle));
        System.out.println(det);
    }
}
