public class Matrix {

    public static final Integer DIAGONAL_MATRIX = 0;
    public static final Integer UPPER_TRIANGLE = 1;
    public static final Integer LOWER_TRIANGLE = -1;


    public static double[][] addTwoArray(double[][] arr1, double[][] arr2) {
        if (arr1 != null && arr2 != null && arr1.length == arr2.length && arr1[0].length == arr2[0].length) {
            double[][] combinedArray = new double[arr1.length][arr1[0].length];
            for (int i = 0; i < arr1.length; i++) {
                for (int j = 0; j < arr1[i].length; j++) {
                    combinedArray[i][j] = arr1[i][j]+ arr2[i][j];
                }
            }
            return combinedArray;
        } else {
            throw new IllegalArgumentException("The array should not been null and the length should be the same");
        }
    }

    public static double[][] scalarProduct(double[][] arr, Double factor) {
        if (arr == null || factor == null)
            throw new IllegalArgumentException("Array Should not be null");
        double[][] scaledArray = new double[arr.length][arr[0].length];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                scaledArray[i][j] = arr[i][j] * factor;
            }
        }
        return scaledArray;
    }


    public static double[][] transportation(double[][] arr) {
        if (arr == null)
            throw new IllegalArgumentException("Array Should not be null");
        double[][] transportArray = new double[arr[0].length][arr.length];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                transportArray[j][i] = arr[i][j];
            }
        }
        return transportArray;
    }


    public static double[][] multiplyTwoArray(double[][] arr1, double[][] arr2) {
        if (arr1 == null || arr2 == null)
            throw new IllegalArgumentException("Arrays Should not be null");
        if (arr1[0].length != arr2.length)
            throw new IllegalArgumentException("Incorrect Matrix Size");

        double[][] multipliedArray = new double[arr1.length][arr2[0].length];

        for (int i = 0; i < multipliedArray.length; i++) {
            for (int j = 0; j < multipliedArray[i].length; j++) {
                multipliedArray[i][j] = multiplyCell(arr1, arr2, i, j);
            }
        }
        return multipliedArray;
    }

    public static double[][] subMatrix(double[][] matrix, Integer removedColumn, Integer removedRow) {
        double[][] subMatrix = new double[removedRow == null || removedRow >= matrix.length ? matrix.length : matrix.length - 1][removedColumn == null || removedColumn >= matrix[0].length ? matrix[0].length : matrix[0].length - 1];
        boolean isColumnDeleted = false;
        boolean isRowDeleted = false;
        for (int i = 0; i < matrix.length; i++) {
            isColumnDeleted = false;
            if (removedRow != null && removedRow == i) {
                isRowDeleted = true;
                continue;
            }
            for (int j = 0; j < matrix[i].length; j++) {
                if (removedColumn != null && removedColumn == j) {
                    isColumnDeleted = true;
                    continue;
                }
                int row = isRowDeleted ? i - 1 : i;
                int column = isColumnDeleted ? j - 1 : j;
                subMatrix[row][column] = matrix[i][j];
            }
        }

        return subMatrix;
    }


    public static double[][] squareMatrix(double[][] squareMatrix, int matrixMode) {
        if (squareMatrix.length != squareMatrix[0].length) {
            throw new IllegalArgumentException("Expected Square matrix");
        }
        double[][] squared = new double[squareMatrix.length][squareMatrix.length];

        for (int i = 0; i < squareMatrix.length; i++) {
            for (int j = 0; j < squareMatrix.length; j++) {
                if (i == j) {
                    squared[i][j] = squareMatrix[i][j];

                } else {
                    if (matrixMode == DIAGONAL_MATRIX) {
                        squared[i][j] = 0;
                    } else if (matrixMode == UPPER_TRIANGLE) {
                        if (j < i)
                            squared[i][j] = 0;
                        else
                            squared[i][j] = squareMatrix[i][j];
                    } else if (matrixMode == LOWER_TRIANGLE) {
                        if (j > i)
                            squared[i][j] = 0;
                        else
                            squared[i][j] = squareMatrix[i][j];

                    }
                }
            }
        }
        return squared;
    }

    public static double determination(double[][] squaredMatrix) {
        if (squaredMatrix.length != squaredMatrix[0].length) {
            throw new IllegalArgumentException("Expected Squared Matrix");
        }
        double det = 0;

        if (squaredMatrix.length == 1) {
            det = squaredMatrix[0][0];
        } else if (squaredMatrix.length == 2) {
            det = squaredMatrix[0][0] * squaredMatrix[1][1] - squaredMatrix[0][1] * squaredMatrix[1][0];
        } else {
            for (int i = 0; i < squaredMatrix.length; i++) {
                double factor = Math.pow(-1, i) * squaredMatrix[0][i];
                det += (factor * determination(subMatrix(squaredMatrix, i, 0)));
            }
        }
        return det;
    }


    private static double multiplyCell(double[][] firstMatrix, double[][] secondMatrix, int row, int col) {
        double sum = 0;
        for (int i = 0; i < secondMatrix.length; i++) {
            sum += firstMatrix[row][i] * secondMatrix[i][col];
        }
        return sum;
    }


}
