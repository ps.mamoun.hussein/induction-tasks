package org.example;

import org.example.tree.BinarySearchTree;

public class Main {
    public static void main(String[] args) {
        BinarySearchTree<Integer> bst = new BinarySearchTree<>();
        System.out.println(bst.accept(10));
        System.out.println(bst.accept(18));
        System.out.println(bst.accept(6));
        System.out.println(bst.accept(5));
        System.out.println(bst.accept(2));
        System.out.println(bst.accept(2));
        System.out.println(bst);
        System.out.println(bst.depth(5));
        System.out.println(bst.treeDepth());

    }
}