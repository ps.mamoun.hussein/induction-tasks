package org.example.tree;

import com.sun.istack.internal.NotNull;
import org.example.node.Node;

public class BinarySearchTree<T extends Number> {
    private Node<T> root = null;

    public boolean accept(T value) {
        // base condition
        if (this.root == null) {
            this.root = new Node<>(value);
            return true;
        }
        if (value == null) {
            throw new IllegalArgumentException("Can't add null value to the tree");
        }
        return addToTheTree(root, value);
    }


    public int depth(T value) {
        return search(root, value);
    }

    public int treeDepth() {
        return treeDepth(this.root);
    }

    private int treeDepth(Node<T> root) {
        if (root == null) return 0;
        return Math.max(treeDepth(root.getLeft()), treeDepth(root.getRight())) + 1;
    }

    private Integer search(Node<T> root, T value) {
        if (root == null) {
            return -1;
        }
        if (value.doubleValue() > root.getValue().doubleValue()) {
            int depth = search(root.getRight(), value);
            return depth != -1 ? depth + 1 : depth;
        } else if (root.getValue().doubleValue() > value.doubleValue()) {
            int depth = search(root.getLeft(), value);
            return depth != -1 ? depth + 1 : depth;
        } else {
            return 1;
        }
    }

    private boolean addToTheTree(Node<T> root, T value) {
        if (value.doubleValue() > root.getValue().doubleValue()) {
            if (root.getRight() == null) {
                root.setRight(new Node<>(value));
                return true;
            } else
                return addToTheTree(root.getRight(), value);
        } else if (value.doubleValue() < root.getValue().doubleValue()) {
            if (root.getLeft() == null) {
                root.setLeft(new Node<>(value));
                return true;
            } else
                return addToTheTree(root.getLeft(), value);
        } else {
            return false;
        }
    }


    @Override
    public String toString() {
        return root.toString();
    }
}
