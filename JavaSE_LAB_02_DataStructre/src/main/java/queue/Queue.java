package queue;

import node.Node;
import util.LinkedList;

public class Queue<T> extends LinkedList<T> {

    public Queue(int maxLength) {
        super(maxLength);
    }

    public Queue() {
    }


    public void enqueue(T value) {
        addToList(value);

    }


    public T dequeue() {
        return removeFromList();
    }

    @Override
    protected void addToList(T value) {
        if (this.getMaxSize() > this.size()) {
            Node<T> node = new Node<>(value);
            Node<T> head = this.getHead();
            if (head == null)
                setHead(node);
            else {
                while (head.getNext() != null) {
                    head = head.getNext();
                }
                head.setNext(node);
            }
            setSize(size() + 1);

        } else {
            throw new IllegalArgumentException("Can't add to the queue over the max size");
        }
    }

}
