package util;

import node.Node;

public abstract class LinkedList<T> {
    private Node<T> head = null;
    private int size;
    private final int maxSize;

    public LinkedList(int maxLength) {
        this.maxSize = maxLength;
    }

    public LinkedList() {
        this.maxSize = Integer.MAX_VALUE;
    }

    // implement this the way you that required as stack or queue
    protected abstract void addToList(T value);

    protected T removeFromList() {
        Node<T> pooped = this.getHead();
        this.setHead(pooped != null ? getHead().getNext() : null);
        this.setSize(this.size() - 1);
        return pooped == null ? null : pooped.getValue();
    }




    protected void setSize(int length) {
        this.size = length;
    }

    protected void setHead(Node<T> head) {
        this.head = head;
    }

    public T peek() {
        // return just the value so the user can't edit it
        return this.head.getValue();
    }

    protected Node<T> getHead() {
        return head;
    }

    // return actual item inside the linked list
    public int size() {
        return this.size;
    }

    public int getMaxSize() {
        return maxSize;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("head => ");
        Node<T> node = this.getHead();

        while (node != null) {
            sb.append(node).append(" => ");
            node = node.getNext();
        }
        sb.append("Null");
        return sb.toString();
    }
}
