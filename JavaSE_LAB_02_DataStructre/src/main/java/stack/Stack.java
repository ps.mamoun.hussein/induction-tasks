package stack;

import node.Node;
import util.LinkedList;

public class Stack<T> extends LinkedList<T> {

    public Stack(int maxLength) {
        super(maxLength);
    }

    public Stack() {
        super();
    }


    public void push(T value) {
        addToList(value);
    }

    public T pop() {
        return removeFromList();
    }

    @Override
    protected void addToList(T value) {
        if (getMaxSize() > this.size()) {
            Node<T> node = new Node<>(value);
            node.setNext(this.getHead());
            this.setHead(node);
            this.setSize(this.size() + 1);
        } else {
            throw new IllegalArgumentException("Can't add to the stack over the max size");
        }
    }
}
